/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-item/paper-item-body.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import './my-horizontal-bar-chart-log.js';
import 'paper-collapse-item/paper-collapse-item.js';
import {MyFormatter} from './my-helpers.js';

class MyClassCrossingsEvaluation extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
            }
        </style>
        <iron-ajax
          auto
          with-credentials
          url="[[restBaseUrl]]/classcountingsevaluations"
          handle-as="json"
          on-response="handleClasscountingsevaluationsResponse">
        </iron-ajax>
        <iron-ajax
            id="my-class-countings-evaluation-runner"
            with-credentials
            url="[[restBaseUrl]]/run/classcrossingsevaluation"
            method="post"
            params="{{_getRunClassCountingsEvaluationParams(selectedClasscountingsEvaluation)}}"
            handle-as="json"
            content-type="application/json"
            on-response="handleClassCrossingsEvaluationResult">
        </iron-ajax>
        <div class="card">
            <paper-collapse-item>
                <h2 slot="header">Weiter Auswerten</h2>
                <paper-tabs selected="{{selectedTabIndexClassCrossings}}" scrollable>
                    <paper-tab>Auswertungsmethode</paper-tab>
                    <paper-tab>Code</paper-tab>
                </paper-tabs>
                <iron-pages selected="[[selectedTabIndexClassCrossings]]">
                    <div>
                        <div class="left-part">
                            <paper-listbox class="horizontal" attr-for-selected="item-self" slot="dropdown-content" selected="{{selectedClasscountingsEvaluation}}">
                                <template is="dom-repeat" items="{{availableClasscountingsEvaluations}}">
                                    <paper-item item-self="[[item]]">[[styleStringForTitle(item.name)]]
                                        <a href="[[restBaseUrl]]/html/classcountingsevaluations/[[item.name]].html" target="_blank">
                                            <iron-icon class="blueish-mini" icon="help"/>
                                        </a>
                                    </paper-item>
                                </template>
                            </paper-listbox>
                        </div>

                        <div class="right-part">
                            <template is="dom-repeat" items="{{selectedClasscountingsEvaluation.parameters.params}}">
                                <paper-input label="[[item.name]]" value="{{item.value}}"></paper-input>
                            </template>
                            <paper-button class="blueish" on-click="runClassCountingsEvaluation" disabled="[[_isNotSet(selectedClasscountingsEvaluation)]]"><iron-icon icon="done-all"/></paper-button>
                        </div>
                    </div>
                </iron-pages>
                <template is="dom-if" if="[[_exists(results.resultAsTable)]]">
                    <my-simple-table table-data="[[results.resultAsTable]]" table-header="[[results.tableHeaders]]" hide-headers-left>
                        <p slot="content"><b>Gesamtschaedigung: [[_toPrecision(results.totalDamage,4)]]</b></p>
                    </my-simple-table>
                </template>
            </paper-collapse-item>
        </div>
        `;
    }

    static get properties() {
        return {
            restBaseUrl:{
                type:String,
                value(){
                  return "http://"+location.hostname+":8090";
                }
            },
            results:{
                type: Object,
                value: {},
                notify: true,
            },
            classCrossingsResult:{
                type: Array,
                //value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            classCrossingsResultLabeled:{
                type: Array,
                //value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            availableClasscountingsEvaluations: {
                type: Array,
                value() {
                  return [
                    {name: 'No-Scripts-Loaded', file: 'No-Files',code: 'empty'},
                  ];
                },
                notify: true,
            },
            selectedClasscountingsEvaluation:{
                type: Object,
                notify: true,
            },
            selectedTabIndexClassCrossings:{
                type: Number,
                value: 0,
                notify: true,
                //reflectToAttribute:true,
            },
        }
    }

    static get observers(){
        return [
          '_resetResults(classCrossingsResult,classCrossingsResultLabeled)',
        ];
    }

    styleStringForTitle(stringToStyle){
        return MyFormatter.styleStringForTitle(stringToStyle);
    }

    handleClasscountingsevaluationsResponse(result){
        if(result.returnValue){
            var response = result.detail.response;
            var scripts = response.scripts;
            console.log("set availableClasscountingsEvaluations 1",{scripts});
            this.set("availableClasscountingsEvaluations",scripts);
        }else if(result.detail&&result.detail.__data&&result.detail.__data.status==200){
            var response = result.detail.__data.response;
            var scripts = response.scripts;
            console.log("set availableClasscountingsEvaluations 2",{scripts});
            //console.log("availableClasscountingsEvaluations",response.scripts);
            this.set("availableClasscountingsEvaluations",scripts);
        }else{
            //console.error("Getting classcountingsEvaluations failed");
        }
      }

    _logClassCrossingsResult(base,changeRecord){
        console.log("classCrossingsResult: ",{base});
    }

    _exists(target){
        return target ? true : false;
    }
    
    _computeCalculationClassHeight(changeRecord){
        //console.log("_computeCalculationClassHeight");
        //console.log(changeRecord.base);
        if(changeRecord.base.length==0){
          return 0;
        }
        return ((changeRecord.base.length)*10)+500;
    }

    _getRunClassCountingsEvaluationParams(acces_token) {
        console.log('selected script: %s acces_token: %s',this.get('selectedScript'),acces_token);
        console.log("parameters to JSON:",JSON.stringify(acces_token.parameters.params[0]));
        return {
          'script':acces_token.name,
        };
    }

    runClassCountingsEvaluation() {
        var myAjaxRequester = this.shadowRoot.querySelector('#my-class-countings-evaluation-runner');
        var selectedClasscountingsEvaluation = this.get('selectedClasscountingsEvaluation');
        var classCrossingsResult = this.get('classCrossingsResult');
        var params = selectedClasscountingsEvaluation.parameters;
        console.log({selectedClasscountingsEvaluation,params,classCrossingsResult});
        myAjaxRequester.body={
          calculationClasses:classCrossingsResult,
          parameters:params,
        };
        var ajaxData = myAjaxRequester.body;
        console.log({ajaxData});
        myAjaxRequester.generateRequest();
    }

    _toPrecision(value,precision){
        return Number.parseFloat(value).toPrecision(precision);
    }

    _isNotSet(myObject){
        if(myObject==undefined){
          //console.log("_isNotSet undefined - true");
          return true;
        }
        if(myObject==null){
          //console.log("_isNotSet true");
          return true;
        }
        //console.log("_isNotSet false");
        return false;
    }

    _resetResults(){
        this.set("results",{});
    }

    handleClassCrossingsEvaluationResult(result) {
        if(result.detail.status == 200){
            var response = result.detail.response;
            console.log("class crossing evaluation result:",{response});
            this.set("results.classCrossingsEvaluationResult",response);
            var tableHeaders = ["Stufe (i)","Spannung Sa(i)","Summenhaeufigkeit H(i)","Stufenhaeufigkeit h(i)","Schwingspielzahl N(i)","Schaedigung D(i)","Prozentuelle Schaedigung"];
            this.set("results.tableHeaders",tableHeaders);
            var resultAsTable = response.levels.map(function(v,i){
                return [response.levels[i],response.tensions[i],response.sumAmounts[i],response.levelAmounts[i],response.tolerableNumberOfCycles[i],response.damageParts[i],response.percentDamage[i]];
            })
            this.set("results.totalDamage",response.totalDamage);
            this.set("results.resultAsTable",resultAsTable);
        }else if(result.detail&&result.detail.__data&&result.detail.__data.status==200){
            var response = result.detail.__data.response;
            console.log("class crossing evaluation result:",{response});
            this.set("results.classCrossingsEvaluationResult",response);
            var tableHeaders = ["Stufe (i)","Spannung Sa(i)","Summenhaeufigkeit H(i)","Stufenhaeufigkeit h(i)","Schwingspielzahl N(i)","Schaedigung D(i)","Prozentuelle Schaedigung"];
            this.set("results.tableHeaders",tableHeaders);
            var resultAsTable = response.levels.map(function(v,i){
                return [response.levels[i],response.tensions[i],response.sumAmounts[i],response.levelAmounts[i],response.tolerableNumberOfCycles[i],response.damageParts[i],response.percentDamage[i]];
            })
            this.set("results.totalDamage",response.totalDamage);
            this.set("results.resultAsTable",resultAsTable);
        }else{
            console.error("Script failed",{result});
        }
    }
}

window.customElements.define('my-class-crossings-evaluation', MyClassCrossingsEvaluation);