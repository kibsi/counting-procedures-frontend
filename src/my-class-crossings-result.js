/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import 'paper-collapse-item/paper-collapse-item.js';
import './my-horizontal-bar-chart.js';
import './my-class-crossings-evaluation.js';
import {MyFormatter} from './my-helpers.js';

class MyClassCrossingsResult extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
            }
        </style>
        <div id="containingdiv" width="{{containingWidth}}">
            <template is="dom-if" if="[[_exists(dataObject.results.classcrossingsLabeled)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Klassenhäufigkeit</h2>
                        <paper-tabs selected="{{selectedTabIndexClassCrossings}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexClassCrossings]]">
                            <my-horizontal-bar-chart table-data="[[dataObject.results.classcrossingsLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.classcrossingsLabeled)]]"></my-horizontal-bar-chart>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.classcrossings)]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div>
            </template>
            <template is="dom-if" if="[[_exists(dataObject.results.accumulatedCyclesLabeled)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Summenhäufigkeit</h2>
                        <paper-tabs selected="{{selectedTabIndexAccumulatedCycles}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexAccumulatedCycles]]">
                                <my-horizontal-bar-chart table-data="[[dataObject.results.accumulatedCyclesLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.accumulatedCyclesLabeled)]]"></my-horizontal-bar-chart>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.accumulatedCycles,'name','max','min','amount')]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div>
            </template>
            <template is="dom-if" if="[[_exists(dataObject.results.accumulatedAmplitudesLabeled)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Spannungsamplituden</h2>
                        <paper-tabs selected="{{selectedTabIndexTensionAmplitudes}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexTensionAmplitudes]]">
                            <my-horizontal-bar-chart table-data="[[dataObject.results.accumulatedAmplitudesLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.accumulatedAmplitudesLabeled)]]"></my-horizontal-bar-chart>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.accumulatedAmplitudes,'name','amplitude','min','amount')]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div>
            </template>
            <!-- template is="dom-if" if="[[_exists(dataObject.results.accumulatedAmplitudesLabeled)]]">
                <my-class-crossings-evaluation class-crossings-result-labeled="[[dataObject.results.accumulatedAmplitudesLabeled]]" class-crossings-result="[[dataObject.results.accumulatedAmplitudes]]"></my-class-crossings-evaluation>
            </template -->
            <template is="dom-if" if="[[_exists(dataObject.results.accumulatedCyclesLabeled)]]">
                <my-class-crossings-evaluation class-crossings-result-labeled="[[dataObject.results.accumulatedCyclesLabeled]]" class-crossings-result="[[dataObject.results.accumulatedCycles]]"></my-class-crossings-evaluation>
            </template>
        </div>
        `;
    }

    static get properties() {
        return {
            dataObject:{
                type: Object,
                value: {},
                notify: true,
            },
            selectedTabIndexClassCrossings:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexAccumulatedCycles:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexTensionAmplitudes:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexFromToGrid:{
                type: Number,
                value: 0,
                notify: true,
            },
        }
    }

    static get observers(){
        return [
            '_initDataObject(dataObject)',
            '_computeClassCrossingsLabeled(dataObject.dataClasses)',
            '_computeClassCrossings(dataObject.dataClasses)',
            '_computeAccumulatedCycles(dataObject.dataClasses)',
            '_computeAccumulatedCyclesLabeled(dataObject.dataClasses)',
            //'_computeTensionAmplitudes(dataObject.dataClasses)',
            //'_computeTensionAmplitudesLabeled(dataObject.dataClasses)',
        ];
    }

    _exists(target){
        return target ? true : false;
    }

    _initDataObject(dataObject){
        if(!dataObject.results){
            dataObject.results={};
        }
    }

    _gridFromToTableHeaders(gridFromTo){
        var dataObject = this.get("dataObject");
        console.error("TODO",{dataObject});
    }

    _computeClassCrossingsLabeled(dataClasses){
        if(dataClasses){
            var classcrossingsLabeled = dataClasses.map(entry=>[entry.name,entry.maxLabel,entry.minLabel,entry.amount]);
            console.log("computed: ",{classcrossingsLabeled});
            this.set("dataObject.results.classcrossingsLabeled",classcrossingsLabeled);
        }
    }

    _computeClassCrossings(dataClasses){
        if(dataClasses){
            var classcrossings = dataClasses.map(entry=>{return {name:entry.name,max:entry.max,min:entry.min,amount:entry.amount}});
            console.log("computed: ",{classcrossings});
            this.set("dataObject.results.classcrossings",classcrossings);
        }
    }
    
    _computeCalculationClassHeight(table,changeRecord){
        //console.log("_computeCalculationClassHeight");
        //console.log(changeRecord.base);
        if(changeRecord){
            var base = changeRecord.base;
        }else{
            var base = table;
        }
        if(!base || base.length==0){
          return 0;
        }
        return ((base.length)*36)+100;
    }

    
    /*_computeAccumulatedCycles(dataClasses){
        if(dataClasses){

            var sortedDataClasses = dataClasses.slice().sort((a,b)=>{
                if(Math.abs(a.min) == Math.abs(b.min)){
                    return a.min-b.min;
                }
                return Math.abs(a.min)-Math.abs(b.min);
            })
            console.log({sortedDataClasses});

            var accumulatedPosAmount = 0;
            var accumulatedNegAmount = 0;
            var reversedPositive = dataClasses.slice().filter(v=>v.min>=0).reverse();
            var negatives = dataClasses.slice().filter(v=>v.min<0);

            var accumulatedPositives = reversedPositive.map(function(v,i){
                accumulatedPosAmount = accumulatedPosAmount + v.amount;
                return {name:v.name,max:v.max,min:v.min,amount:accumulatedPosAmount};
            });
            var accumulatedNegatives = negatives.map(function(v,i){
                accumulatedNegAmount = accumulatedNegAmount + v.amount;
                return {name:v.name,max:v.max,min:v.min,amount:accumulatedNegAmount};
            }).reverse();
            var accumulatedCycles = accumulatedPositives.concat(accumulatedNegatives);
            console.log("computed: ",{accumulatedCycles});
            this.set("dataObject.results.accumulatedCycles",accumulatedCycles);
        }
    }
    _computeAccumulatedCyclesLabeled(dataClasses){
        if(dataClasses){
            var accumulatedPosAmount = 0;
            var accumulatedNegAmount = 0;
            var reversedPositive = dataClasses.slice().filter(v=>v.min>=0).reverse();
            var negatives = dataClasses.slice().filter(v=>v.min<0);

            var accumulatedPositivesLabeled = reversedPositive.map(function(v,i){
                accumulatedPosAmount = accumulatedPosAmount + v.amount;
                return [v.name,v.minLabel,v.maxLabel,accumulatedPosAmount];
            });
            var accumulatedNegativesLabeled = negatives.map(function(v,i){
                accumulatedNegAmount = accumulatedNegAmount + v.amount;
                return [v.name,v.minLabel,v.maxLabel,accumulatedNegAmount];
            }).reverse();
            var accumulatedCyclesLabeled = accumulatedPositivesLabeled.concat(accumulatedNegativesLabeled);

            console.log("computed: ",{accumulatedCyclesLabeled});
            this.set("dataObject.results.accumulatedCyclesLabeled",accumulatedCyclesLabeled);
        }
    }*/

    _computeAccumulatedCycles(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().sort((a,b)=>{
                if(Math.abs(a.max) == Math.abs(b.max)){
                    return b.max-a.max;
                }
                return Math.abs(b.max)-Math.abs(a.max);
            });
            console.log({reversed});
            var accumulatedCycles = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                return {name:v.name,max:v.max,min:v.min,amount:accumulatedAmount};
            });
            console.log("computed: ",{accumulatedCycles});
            this.set("dataObject.results.accumulatedCycles",accumulatedCycles);
        }
    }
    _computeAccumulatedCyclesLabeled(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().sort((a,b)=>{
                if(Math.abs(a.max) == Math.abs(b.max)){
                    return b.max-a.max;
                }
                return Math.abs(b.max)-Math.abs(a.max);
            });
            console.log({reversed});
            var accumulatedCyclesLabeled = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                return [v.name,v.minLabel,v.maxLabel,accumulatedAmount];
            });
            console.log("computed: ",{accumulatedCyclesLabeled});
            this.set("dataObject.results.accumulatedCyclesLabeled",accumulatedCyclesLabeled);
        }
    }
    

    /*
    _computeTensionAmplitudes(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedAmplitudes = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                let amplitude = Math.abs(v.max/2);
                return {name:v.name,max:amplitude,min:0,amount:accumulatedAmount};
            });
            console.log("computed: ",{accumulatedAmplitudes});
            this.set("dataObject.results.accumulatedAmplitudes",accumulatedAmplitudes);
        }
    }

    _computeTensionAmplitudesLabeled(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedAmplitudesLabeled = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                let amplitude = Math.abs(v.max/2).toPrecision(4);
                return [v.name,"Amplitude: "+amplitude,accumulatedAmount];
            });
            console.log("computed: ",{accumulatedAmplitudesLabeled});
            this.set("dataObject.results.accumulatedAmplitudesLabeled",accumulatedAmplitudesLabeled);
        }
    }
    */

    _ArrayToText(array,...headerArray){
        if(!array || array.length <1){
            return;
        }
        let containsArrays = Array.isArray(array[0]);
        if(containsArrays){
            var headerString="";
            if(headerArray && headerArray.length>0){
                console.log({headerArray});
                headerString = headerArray.map(v=>MyFormatter._capitalize(v)).join(";")+"\n";
            }
            return headerString+array.map(function(v){
                return v.join(";")+"\n";
            }).join("");
        }else{
            var firstElement = array[0];
            var keys = Object.keys(firstElement);
            console.log({keys,firstElement});
            var header = keys.map(v=>MyFormatter._capitalize(v)).join(";")+";\n";
            console.log({header});
            var body = array.map(row=>keys.map(key=>row[key]).join(";")+";\n").join("");
            console.log({body});
            return header.concat(body);
        }
    }

    _ArrayToIndexHeaders(array){
        if(array){
            return array.map(function(v,i){
                return i;
            });
        }
    }
}

window.customElements.define('my-class-crossings-result', MyClassCrossingsResult);