/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import * as hljs from 'highlightjs/highlight.pack.js';

class MyCodeHighlighter extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
                padding: 10px;
            }
        </style>
        <div>
            <pre><code><slot></slot></code></pre>
        </div>
        `;
    }

    static get properties() {
        return {
        }
    }

    static get observers(){
        return [
          //'_handleResultsChange(result)',
          //'_markovProbabilitiesToString(markovProbabilities.*)',
        ];
    }

    ready(){
        super.ready();
        this.shadowRoot.querySelector()
    }
}

window.customElements.define('my-code-highlighter', MyCodeHighlighter);