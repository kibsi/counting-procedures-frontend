/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import './my-range-count-result.js';
import './my-class-crossings-result.js';
import './my-markov-result.js';
import './my-rainflow-result.js';

class MyCountingProcedureResult extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
            }
        </style>
        <template is="dom-if" if="[[dataObject.hasClassCrossings]]">
            <my-class-crossings-result data-object = "[[dataObject]]"></my-class-crossings-result>
        </template>
        <template is="dom-if" if="[[dataObject.hasRainflow]]">
            <my-rainflow-result data-object = "[[dataObject]]"></my-rainflow-result>
        </template>
        <template is="dom-if" if="[[dataObject.hasRangecounts]]">
            <my-range-count-result data-object = "[[dataObject]]"></my-range-count-result>
        </template>
        <template is="dom-if" if="[[dataObject.hasMarkov]]">
            <my-markov-result data-object = "[[dataObject]]"></my-markov-result>
        </template>
        
        `;
    }

    static get properties() {
        return {
            dataObject:{
                type: Object,
                value: {},
                notify: true,
            },
            result: {
                type: Object,
                notify: true,
                reflectToAttribute: true,
            },
            rangeCounts:{
                type:Array,
                notify: true,
                //reflectToAttribute:true,
            },
            calculationClassHeight:{
                type: Number,
                computed: "_computeCalculationClassHeight(dataObject.dataClasses)",
                notify: true,
                //reflectToAttribute:true,
            },
            classCrossingsResult:{
                type: Array,
                value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            calculationClasses:{
                type: Array,
                notify: true,
                //reflectToAttribute:true,
            },
            selectedTabIndexClassCrossings:{
                type: Number,
                value: 0,
                notify: true,
                //reflectToAttribute:true,
            },
            yLines:{
                type: Array,
                notify: true,
                //reflectToAttribute:true,
            },
            markovProbabilities: {
                type: Array,
                notify: true,
                value: [],
                //reflectToAttribute:true,
            },
            
            markovProbabilitiesString: {
                type: String,
                notify: true,
                //reflectToAttribute:true,
            },
            markovProbSelection: {
                type: String,
                value: "0",
                notify: true,
                //reflectToAttribute:true,
            },
            markovN: {
                type: Number,
                value: 100,
                notify: true,
                //reflectToAttribute:true,
            },
            markovGeneratedValues: {
                type: Array,
                value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            usedIndizes: {
                type: Array,
                value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            pointsOfInterest: {
                type: Array,
                value: [[]],
                notify: true,
                //reflectToAttribute:true,
            },
            tableValues: {
                type: Array,
                //value() {
                //  return [{time:'5',value:"6"},{time:'10',value:'12'},{time:'15',value:'7'}];
                //},
                notify:true,
                //reflectToAttribute:true,
                //observers :
            },
            gridFromTo:{
                type: Array,
                value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            classHeaders:{
                type: Array,
                notify: true,
            },
        }
    }

    static get observers(){
        return [
          '_extractResults(result)',
          //'_resetResults(tableValues,tableValues.*)',
          //'_calculateProbabilities(gridFromTo,gridFromTo.*)',
        ];
    }

    

    _isEmpty(array,changeRecord){
        if(array==undefined){
          //console.log("_isEmpty undefined - true");
          return true;
        }
        if(array.length>0){
          //console.log("_isEmpty false");
          return false;
        }
        //console.log("_isEmpty true");
        return true;
    }
    
    _computeCalculationClassHeight(dataClasses){
        //console.log("_computeCalculationClassHeight");
        //console.log(changeRecord.base);
        if(dataClasses.length==0){
          return 0;
        }
        return ((dataClasses.length)*100)+200;
    }

    _calculateProbabilities(matrix,changeRecord){
        var gridFromTo = this.get("gridFromTo");
        console.log("gridFromTo",{gridFromTo});
        var probabilities = gridFromTo.map(function(rowValues){
            var totalsPerRow = rowValues.reduce((pv, cv) => pv+cv, 0);
            if(totalsPerRow>0){
                return rowValues.map(function(cellValue){
                    return cellValue/totalsPerRow;
                })
            }
            return rowValues;
        });
        console.log("generated Probs:",{probabilities});
        var markovHeaders =  probabilities.map((v,i) => i);
        this.set("markovProbabilities",probabilities);
        //this.set("markovProbabilitiesHeader",markovHeaders);
    }
}



window.customElements.define('my-counting-procedure-result', MyCountingProcedureResult);