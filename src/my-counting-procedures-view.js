/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/polymer/lib/elements/dom-bind.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
// import {DomIf as DomIf} from '@polymer/polymer/lib/elements/dom-if.js';
// Import array selector
import '@polymer/polymer/lib/elements/array-selector.js';
import '@vaadin/vaadin-grid/all-imports.js';
import 'paper-collapse-item/paper-collapse-item.js';
//import * as d3 from "d3";
//import * as ace from 'ace-builds/src/ace.js';
//import 'ace/mode/python';
//import 'ace/theme/monokai';
import './shared-styles.js';
import './my-table-data-info.js';
import './my-table-data-view.js';
import './my-xy-chart.js';

import './my-counting-procedure-result.js';
import {MyFormatter} from './my-helpers.js';
import {MyResultConverter} from './my-result-converter.js';

class MyView1 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
      </style>
      <iron-ajax
          auto
          with-credentials
          url="[[restBaseUrl]]/examples"
          handle-as="json"
          on-response="handleExamplesResponse"></iron-ajax>
      <iron-ajax
          auto
          with-credentials
          url="[[restBaseUrl]]/example"
          params="[[_getExampleParams(selectedExample)]]"
          handle-as="text"
          on-response="handleExampleDataResponse"></iron-ajax>
      <iron-ajax
          auto
          with-credentials
          url="[[restBaseUrl]]/scripts"
          handle-as="json"
          on-response="handleScriptsResponse">
      </iron-ajax>
      <iron-ajax
            id="my-script-runner"
            with-credentials
            url="[[restBaseUrl]]/run"
            method="post"
            params="{{_getRunScriptParams(selectedScript)}}"
            handle-as="json"
            content-type="application/json"
            on-response="handleScriptResult">
      </iron-ajax>
      
      <div class="card">
        <!-- <div class="circle">1</div> -->
        <h1 class="blueish">{{styleStringForTitle(selectedScript.name)}}</h1>
        <!-- <p>Einparametrige Zählverfahren haben nur einen Parameter der gemessen wird</p> -->
        <!-- <p>Ein typisches Beispiel ist die Klassengrenzenüberschreitungszählung</p> -->
      </div>

      <div class= "card">
        <paper-tabs selected="{{selectedTabIndex}}" scrollable>
          <paper-tab>Daten</paper-tab>
          <paper-tab disabled="[[_isEmpty(sourceTableValues,sourceTableValues.*)]]">Auswertungsmethoden</paper-tab>
          <!-- <paper-tab>Parameter</paper-tab> -->
        </paper-tabs>

        <iron-pages selected="[[selectedTabIndex]]">

          <div class="innercard">
            <my-table-data-view table-values="{{sourceTableValues}}" table-values-as-text="{{dataAsText}}" table-headers="{{tableHeaders}}" examples="[[examples]]" selected-example={{selectedExample}}></my-table-data-view>
            <paper-button class="blueish" on-click="_increaseTabIndex"><iron-icon icon="arrow-forward"></iron-icon></paper-button>
          </div>

          <div>
            <paper-tabs selected="{{selectedTabIndexProcedures}}" scrollable>
              <paper-tab>Auswerteverfahren</paper-tab>
              <paper-tab>Code</paper-tab>
            </paper-tabs>
            <div>

              <iron-pages selected="[[selectedTabIndexProcedures]]">

                <div>

                  <div class="left-part">
                    <paper-listbox class="horizontal" attr-for-selected="item-self" slot="dropdown-content" selected="{{selectedScript}}">
                      <template is="dom-repeat" items="{{availableScripts}}">
                        <paper-item class="selectable" item-self="[[item]]">[[styleStringForTitle(item.name)]]
                          <a href="[[restBaseUrl]]/html/countingprocedures/[[item.name]].html" target="_blank">
                            <iron-icon class="blueish-mini space-left help" icon="help"/>
                          </a>
                        </paper-item>
                      </template>
                    </paper-listbox>
                  </div>

                  <div class="right-part">
                    <my-table-data-info table-data="{{sourceTableValues}}" min="{{viewMinimum}}" max="{{viewMaximum}}"></my-table-data-info>
                    <template is="dom-repeat" items="{{selectedScript.parameters.params}}">
                      <paper-input label="[[item.name]]" value="{{item.value}}"></paper-input>
                    </template>
                    <paper-button class="blueish" on-click="runScript" disabled="[[_isNotSet(selectedScript)]]"><iron-icon icon="done-all"></iron-icon></paper-button>
                  </div>

                </div>

                <div>
                  <paper-textarea value="[[selectedScript.code]]" max-rows="16" readonly></paper-textarea>
                </div>

              </iron-pages>

            </div>
            <div class="next-line"><paper-button class="blueish" on-click="_decreaseTabIndex"><iron-icon icon="arrow-back"></iron-icon></paper-button></div>
          </div>
        </iron-pages>
      </div>
      <div class="card">
        <paper-collapse-item opened>
          <h2 slot="header">Beanspruchungs Zeit Funktion</h2>
          <my-xy-chart data-object="[[dataObject]]" height="300"></my-xy-chart>
        </paper-collapse-item>
      </div>
      <template is="dom-if" if="[[dataObject.ready]]">
      <my-counting-procedure-result data-object="[[dataObject]]"></my-counting-procedure-result>
    </template>
    `;
  }

  static get properties() {
    return {
      restBaseUrl:{
        type:String,
        value(){
          return "http://"+location.hostname+":8090";
        }
      },
      updatingMap: {
        type: Object,
        value: {
          dataAsTextUpdate: {
          type: Boolean,
          value: false,
          notify: true,
          reflectToAttribute:true,
          }
        },
        notify: true,
        reflectToAttribute:true,
      },
      availableScripts: {
        type: Array,
        value() {
          return [
            {name: 'No-Scripts-Loaded', file: 'No-Files',code: 'empty'},
          ];
        },
        notify: true
      },
      dataObject:{
        type: Object,
        value: {},
        notify: true,
      },
      selectedScript:{
        type: Object,
        notify: true,
      },
      examples: {
        type: Array,
        value: [],
        notify: true,
        reflectToAttribute: true,
      },
      selectedExample: {
        type: Object,
        notify: true,
        value: {
          name: 'nothing-selected',
        },
        reflectToAttribute: true,
      },
      tableHeaders: {
        type: Array,
        value(){
          return [];
        }
      },
      sourceTableValues: {
        type: Array,
        value() {
          return [];
        },
        notify:true,
        reflectToAttribute:true,
      },
      selectedTabIndex: {
        type: String,
        value: "0",
        notify: true,
        reflectToAttribute:true,
      },
      selectedTabIndexProcedures: {
        type: String,
        value: "0",
        notify: true,
        reflectToAttribute:true,
      },
      sourceHeadersAsString:{
        type: String,
        value: "time;value;",
      },
      dataAsText:{
        type: String,
        value: "",
        reflectToAttribute:true,
      },
      // flag for not running into endless update cycles
      sourceDataChanged: {
        type: Boolean,
        value: true,
      },
      sourceDataObject:{
        type: Object,
      },
      viewMinimum: {
        type:Number,
        value: 0,
      },
      viewMaximum: {
        type: Number,
        value: 0,
      },
      usedIndizes: {
        type: Array,
        value: [],
      },
      pointsOfInterest: {
        type: Array,
        value: [],
        notify: true,
        reflectToAttribute:true,
      },
    };
  }

  static get observers(){
    return [
      '_sourceDataChanged(sourceTableValues)',
      '_scriptChanged(selectedScript)',
      //'_arrayToCSV(sourceTableValues.*)',
      //'_logHeaderChange(tableHeaders.*)',
      //'_sourceDatatoObject(sourceTableValues.*)',
      //'_refreshSourceCharts(sourceDataObject)',

    ];
  }

  _sourceDataChanged(sourceTableValues){
    console.log("sourceData changed",{sourceTableValues});
    var dataObject = {};
    dataObject.ready = false;
    let dataPoints = sourceTableValues.map((v,i)=>{return{x:i,y:Number.parseFloat(v)}});
    dataObject.dataPoints=dataPoints;
    let lines = dataPoints.slice(0,dataPoints.length-1);
    lines = lines.map((v,i)=>{return{x1:v.x,y1:v.y,x2:dataPoints[i+1].x,y2:dataPoints[i+1].y}});
    dataObject.lines=lines;
    console.log({dataObject});
    this.set("dataObject",dataObject);
  }

  _scriptChanged(script){
    console.log("script changed",{script});
    var oldDataObject = this.get("dataObject");
    var dataObject = {};
    dataObject.ready = false;
    let dataPoints = oldDataObject.dataPoints;
    dataObject.dataPoints=dataPoints;
    let lines = dataPoints.slice(0,dataPoints.length-1);
    lines = lines.map((v,i)=>{return{x1:v.x,y1:v.y,x2:dataPoints[i+1].x,y2:dataPoints[i+1].y}});
    dataObject.lines=lines;
    console.log({dataObject});
    this.set("dataObject",dataObject);
  }

  _extractResults(results){
    if(results){
        this.set("dataObject.results",{});
        console.log({results});
        var dataObject = {};
        dataObject.results={};
        dataObject.ready = true;
        dataObject.dataClasses = {};
        let calculationClasses = results.calculationClasses;
        console.log({calculationClasses});
        if(calculationClasses && Array.isArray(calculationClasses) && calculationClasses.length > 0){
            dataObject.dataClasses = calculationClasses;
            let indizes = calculationClasses.map((v,i)=>{
              var index = i;
              var viewIndex = i+1;
              return {index,viewIndex};
            });
            dataObject.dataClasses=MyResultConverter.combineObjects(dataObject.dataClasses,indizes);
            let labelsArray =  MyResultConverter.objectsToArray(dataObject.dataClasses,["viewIndex","min","max","range"],["Klasse: ","Min: ","Max: ","Range: "],[],[(index)=>padLeft(index,2,"0"),(v)=>Number.parseFloat(v).toPrecision(4),(v)=>Number.parseFloat(v).toPrecision(4)]);
            console.log({labelsArray});
            let labelsObjects = MyResultConverter.arrayToObjects(["name","minLabel","maxLabel","rangeLabel"],labelsArray);
            console.log({labelsObjects});
            dataObject.dataClasses=MyResultConverter.combineObjects(dataObject.dataClasses,labelsObjects);
            console.log({dataObject});
            let yLines = [calculationClasses[0].min].concat(calculationClasses.map(v=>v.max));
            dataObject.yLines=yLines;
        }else{
            return;
        }
        let classCrossings = results.classCrossings;
        if(classCrossings && Array.isArray(classCrossings) && classCrossings.length > 0){
            let classCrossingsObject = MyResultConverter.arrayToObjects(["amount"],classCrossings);
            console.log("Converted array to object:",{classCrossings,classCrossingsObject});
            dataObject.dataClasses = MyResultConverter.combineObjects(dataObject.dataClasses,classCrossingsObject);
            console.log("Combined objects to:",{dataObject});
            dataObject.hasClassCrossings = true;
        }else{
          dataObject.hasClassCrossings = false;
        }
        let sourceTableValues = this.get("sourceTableValues");
        let dataPoints = sourceTableValues.map((v,i)=>{return{x:i,y:Number.parseFloat(v)}});
        dataObject.dataPoints=dataPoints;
        let lines = dataPoints.slice(0,dataPoints.length-1);
        lines = lines.map((v,i)=>{return{x1:v.x,y1:v.y,x2:dataPoints[i+1].x,y2:dataPoints[i+1].y}});
        dataObject.lines=lines;
        let pointsOfInterest = results.pointsOfInterest;
        if(pointsOfInterest && Array.isArray(pointsOfInterest) && pointsOfInterest.length > 0){
            pointsOfInterest = pointsOfInterest.filter(entry=>entry.length>0);
            dataObject.pointsOfInterest = pointsOfInterest;
        }
        let usedIndizes = results.usedIndizes;
        usedIndizes = usedIndizes.map(i=>dataPoints[i]);
        if(usedIndizes && Array.isArray(usedIndizes) && usedIndizes.length > 0){
            dataObject.usedIndizes = usedIndizes;
        }
        let gridFromTo = results.gridFromTo;
        if(gridFromTo && Array.isArray(gridFromTo) && gridFromTo.length > 1){
            dataObject.gridFromTo = gridFromTo;
            dataObject.hasRainflow = true;
        }else{
          dataObject.hasRainflow = false;
        }
        let markovProbabilities =results.markovProbabilities;
        if(markovProbabilities && Array.isArray(markovProbabilities) && markovProbabilities.length > 1){
            dataObject.markovProbabilities = markovProbabilities;
            dataObject.hasMarkov = true;
        }else{
          dataObject.hasMarkov = false;
        }
        let rangeCounts = results.rangeCounts;
        if(rangeCounts && Array.isArray(rangeCounts) && rangeCounts.length > 0){
          let rangeCountsObject = MyResultConverter.arrayToObjects(["amount"],rangeCounts);
            console.log("Converted array to object:",{rangeCounts,rangeCountsObject});
            dataObject.dataClasses = MyResultConverter.combineObjects(dataObject.dataClasses,rangeCountsObject);
            console.log("Combined objects to:",{dataObject});
            //dataObject.rangeCounts = rangeCounts;
            dataObject.yLines = null;
            dataObject.hasRangecounts = true;
        }else{
          dataObject.hasRangecounts = false;
        }
        console.log({dataObject});
        this.set("dataObject",dataObject);
    }
}

  styleStringForTitle(stringToStyle){
    return MyFormatter.styleStringForTitle(stringToStyle);
  }

  //_sourceDataChanged(access_token){
  //  this.set('sourceDataChanged',true);
  //}

  handleScriptsResponse(result){
    if(result.returnValue){
      this.set("availableScripts",result.detail.response.scripts);
    }else if(result.detail&&result.detail.__data&&result.detail.__data.status==200){
      var response = result.detail.__data.response;
      console.log("script result:",response.scripts);
      this.set("availableScripts",response.scripts);
    }else{
      console.error("Getting counting-procedures failed:",{result});
      console.log("status:",result.detail.__data.status)
      console.log("status==200:",result.detail.__data.status==200)
    }
  }

  handleScriptResult(result) {
    if(result.detail.status == 200){
      var response = result.detail.response;
      //console.log("Script delivered result:")
      console.log("script result:",response);
      this._extractResults(response);
    }else if(result.detail&&result.detail.__data&&result.detail.__data.status==200){
      var response = result.detail.__data.response;
      console.log("script result:",response);
      this._extractResults(response);
    }else{
      console.error("Getting result of script failed:",{result});
      console.log("status:",result.detail.__data.status)
      console.log("status==200:",result.detail.__data.status==200)
    }
  }

  handleExamplesResponse(result){
    if(result.returnValue){
      var response = result.detail.response;
      this.set("examples",response.examples);
      this.set("selectedExample",response.examples[0]);
      console.log("Loaded examples",this.get("examples"));
      console.log("Set selected example",this.get("selectedExample"));
    }else if(result.detail&&result.detail.__data&&result.detail.__data.status==200){
      var response = result.detail.__data.response;
      this.set("examples",response.examples);
      this.set("selectedExample",response.examples[0]);
    }else{
      console.error("Getting examples failed:",{result});
      console.log("status:",result.detail.__data.status)
      console.log("status==200:",result.detail.__data.status==200)
    }
  }

  handleExampleDataResponse(result){
    if(result.returnValue){
      //console.log(result.detail.response);
      //console.log("Loaded example data:");
      var responseText = result.detail.response
      console.log("exampledata -> dataAsText:",{responseText});
      this.set('dataAsText',responseText);
      console.log("loaded example data");

    }else if(result.detail&&result.detail.__data&&result.detail.__data.status==200){
      var response = result.detail.__data.response;
      console.log("exampledata -> dataAsText:",{response});
      this.set('dataAsText',response);
    }else{
      console.error("Loading Example Data failed",{result});
      console.log("status:",result.detail.__data.status)
      console.log("status==200:",result.detail.__data.status==200)
    }
  }

  _getRunScriptParams(acces_token) {
    console.log('selected script: %s acces_token: %s',this.get('selectedScript'),acces_token);
    console.log("parameters to JSON:",JSON.stringify(acces_token.parameters.params[0]));
    return {
      'script':acces_token.name,
    };
  }

  _getExampleParams(acces_token) {
    console.log('selected example: %s acces_token: %s',this.get('selectedExample'),acces_token);
    return {'example':acces_token.name};
  }

  runScript() {

    var myAjaxRequester = this.shadowRoot.querySelector('#my-script-runner');
    var selectedScript = this.get('selectedScript');
    console.log("selectedScript:",selectedScript);
    var params = selectedScript.parameters;
    console.log("script params:",params);
    myAjaxRequester.body={
      data:this.get('dataAsText'),
      parameters:params,
    };
    var scriptData = myAjaxRequester.body.data
    console.log("script data:",{scriptData});
    myAjaxRequester.generateRequest();
  }

  _increaseTabIndex(){
    this.set("selectedTabIndex",this.get("selectedTabIndex")+1);
  }

  _decreaseTabIndex(){
    this.set("selectedTabIndex",this.get("selectedTabIndex")-1);
  }

  _isEmpty(array,changeRecord){
    if(array==undefined){
      //console.log("_isEmpty undefined - true");
      return true;
    }
    if(array.length>0){
      //console.log("_isEmpty false");
      return false;
    }
    //console.log("_isEmpty true");
    return true;
}

_isNotSet(myObject){
  if(myObject==undefined){
    //console.log("_isNotSet undefined - true");
    return true;
  }
  if(myObject==null){
    //console.log("_isNotSet true");
    return true;
  }
  //console.log("_isNotSet false");
  return false;
}

_isPresent(array,changeRecord){
  if(array==undefined){
    //console.log("_isPresent undefined - false");
    return false;
  }
  if(array.length>0){
    //console.log("_isPresent true");
    return true;
  }
  //console.log("_isPresent false");
  return false;
}

}

function padLeft(nr, n, str){
  //console.log("padding left");
  var nrLength = String(nr).length;
  if(nrLength>n){
    return nr;
  }
  return Array(n-nrLength+1).join(str||'0')+nr;
}

function padRight(nr, n, str){
  //console.log("padding left");
  var nrLength = String(nr).length;
  if(nrLength>n){
    return nr;
  }
  return nr+Array(n-nrLength+1).join(str||'0');
}



window.customElements.define('my-counting-procedures-view', MyView1);
