/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
//import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
//import '@polymer/polymer/lib/elements/dom-bind.js';
import '@polymer/paper-input/paper-input.js';
//import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-scroll-target-behavior/iron-scroll-target-behavior.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
//import '@polymer/paper-tabs/paper-tabs.js';
//import '@polymer/paper-tabs/paper-tab.js';
//import '@polymer/iron-pages/iron-pages.js';
// Import template repeater
import '@polymer/polymer/lib/elements/dom-repeat.js';
//import '@polymer/polymer/lib/elements/dom-if.js';
// Import array selector
//import '@polymer/polymer/lib/elements/array-selector.js';
//import '@polymer/iron-icon/iron-icon.js';
//import '@vaadin/vaadin-grid/all-imports.js';
//import '@vaadin/vaadin-upload/vaadin-upload.js';
//import '@vaadin/vaadin-grid/vaadin-grid.js';
//import '@vaadin/vaadin-grid/vaadin-grid-column.js';
import './shared-styles.js';
//import {MyFormatter} from './my-helpers.js';

class MyDataGenerator extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
                padding: 10px;
            }
        </style>
        <div class="card">
            <paper-input label="N" value="{{initN}}"></paper-input>
                <div id="scrollable-exampledata" class="scroll">
                    <x-element scroll-target="scrollable-exampledata">
                        <template id="modifierRepeater" is="dom-repeat" items="{{myDataModifiers}}">
                            <div class="grid-container">
                                <div class="grid-top">
                                    <p>{{item.operation}}</p>
                                </div>
                                <div class="grid-center">
                                    <paper-input class="grid-item" label="k (Steigung)" value="{{item.kmin}}"></paper-input>
                                    <paper-input label="d (Abstand y-Achse)" value="{{item.dmin}}"></paper-input>
                                </div>
                                <div class="grid-center">
                                    <paper-input label="A (Amplitude)" value="{{item.Amin}}"></paper-input>
                                    <paper-input label="B (Amplituden Multiplikator)" value="{{item.Bmin}}"></paper-input>
                                </div>
                                <div class="grid-center">
                                    <paper-input label="ω [°] (Schwingungsfrequenz - Wie viele Schwingungen pro 360°)" value="{{item.omegamin}}"></paper-input>
                                    <paper-input label="φ [°] (Schwingungs Abstand)" value="{{item.phimin}}"></paper-input>
                                </div>
                                <div class="grid-bottom">
                                    <paper-button class="blueish" on-click="_removeModifier" data-args="[[index]]"><iron-icon icon="delete"></iron-icon></paper-button>
                                </div>
                            </div>
                        </template>
                    </x-element>
                </div>
            <paper-button class="blueish" on-click="_addModifier">+</paper-button>
            <paper-button class="blueish" on-click="_generate">Generate</paper-button>
        </div>
        `;
    }

    static get properties() {
        return {
            generatedData: {
                type: Array,
                value: [],
                notify: true,
                reflectToAttribute:true,
            },
            initN: {
                type: Number,
                value: 360,
                notify: true,
                //reflectToAttribute:true,
            },
            myDataModifiers:{
                type: Array,
                value: [],
                notify: true,
                //reflectToAttribute:true,
            }
            
        }
    }

    static get observers(){
        return [
        ];
    }

    _addModifier(){
        var length = this.get("myDataModifiers").length;
        if(length==0){
            this.push("myDataModifiers",{
                operation:"y(i+1) = y[i] + k*t+d + ( A + B*y[i] )*sin( ω*t*(π/180) + φ*(π/180) )",
                Amin:"100",
                Bmin:"1",
                omegamin:36/(length+1),
            });
        }else{
            this.push("myDataModifiers",{
                operation:'y(i+1) = y[i] + k*t+d + ( A + B*y[i] )*sin( ω*t*(π/180) + φ*(π/180) )',
                Amin:"0",
                Bmin:"1",
                omegamin:36/(length+1),
            });
        }
    }

    _removeModifier(mouseEvent){
        var indexToRemove = mouseEvent.path[0].dataArgs;
        //var currentModifiers = this.get("myDataModifiers");
        //currentModifiers.slice(index);
        console.log("attempt removing index: ",indexToRemove);
        console.log(this.splice);
        this.splice("myDataModifiers",indexToRemove,1);
        //this.set("myDataModifiers",currentModifiers);
        //this.notifyPath("myDataModifiers");
        console.log("_removeModifier index=",indexToRemove);
        //console.log("myDataModifiers: ",this.get("myDataModifiers"));
    }

    _generate(){
        var amount = this.get("initN");
        var newData = [];
        var dataModifiers = this.get("myDataModifiers");
        console.log("dataModifiers",dataModifiers);
        newData.length = amount;
        newData=newData.fill(0);
        /*.map((v, index) => {
            console.log("v={} index={}",v,index);
            return index;
        });*/
        console.log("newData:",newData);
        dataModifiers.forEach(dataModifier => {
            console.log("dataModifier",dataModifier);
            for(var i=0;i<amount;i++){
                var kd = newData[i];
                if(dataModifier.kmin){
                    //console.log("found k",k);
                    var k = parseFloat(dataModifier.kmin);
                    kd=k*i;
                }
                if(dataModifier.dmin){
                    //console.log("found d",d);
                    var d = parseFloat(dataModifier.dmin);
                    kd=kd+d;
                }
                var AB = newData[i];
                if(dataModifier.Bmin){
                    var B = parseFloat(dataModifier.Bmin);
                    AB = AB * B;
                }
                if(dataModifier.Amin){
                    var A = parseFloat(dataModifier.Amin);
                    AB = AB + A;
                }
                var ωφ = newData[i];
                if(dataModifier.omegamin){
                    var ω = parseFloat(dataModifier.omegamin);
                    ωφ = (i*ω*Math.PI)/180;
                }
                if(dataModifier.phimin){
                    var φ = parseFloat(dataModifier.phimin);
                    ωφ = ωφ + (φ * Math.PI/180)
                }
                newData[i] = newData[i]+kd+AB*Math.sin(ωφ);

            }
        });
        newData = newData.map(value => [value]);
        console.log("newData:",newData);
        this.set("generatedData",newData);
        //this.notifyPath("generatedData");
    }
    
};

window.customElements.define('my-data-generator', MyDataGenerator);