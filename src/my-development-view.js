/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class MyView2 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <div class="card">
        <h1>How to Experiment</h1>
        <p>Since this homepage is licenced under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (see bottom of page),</p>
        <p>you are free to fork and modify the homepage to your needs. (For Details look up the License)</p>
        <p>You can run this homepage on your own Computer!</p>
        <p>You may want to do this to experiment with the code, add or modify algorithms, or improve the graphical user interface.</p>
        <p>Also if you have large amounts of Data, it's faster to have the server running locally.</p>
      </div>
      <div class="card">
        <h1>Use Docker!</h1>
        <p>Docker is used for container-virtualization <a target="_blank" rel="noopener noreferrer" href="https://de.wikipedia.org/wiki/Docker_(Software)">read more on Wikipedia</a></p>
        <p>Docker allows you to run the homepage without needing many programming Skills</p>
        <p>Once you have installed <a target="_blank" rel="noopener noreferrer" href="https://www.docker.com/get-started">Docker from their homepage</a></p>
        <p>you can run the following commands to get the servers running:</p>
        <p><code>docker run -d --rm --name SOME_NAME_FOR_FRONTEND -p 80:8081 kinast/counting-procedures-frontend:latest</code></p>
        <p><code>docker run -d --rm --name SOME_NAME_FOR_BACKEND -p 8090:8090 kinast/counting-procedures-backend:latest</code></p>
        <p>Look up the <a target="_blank" rel="noopener noreferrer" href="https://docs.docker.com/engine/reference/run/">details for the run parameters</a> to run the server with different options</p>
        <p>If you want to make sure it runs and you want to see the logs you maybe want to use the <code>-it</code> (interactive and terminal) option instead of the <code>-d</code> (detached) option</p>
        <p>You maybe also don't always run the latest version of the server but a stable one. Replace the latest version with a stable version tag like "1.0".</p>
        <p>The tags can be found on Docker Hub for the <a target="_blank" rel="noopener noreferrer" href="https://cloud.docker.com/repository/docker/kinast/counting-procedures-frontend/tags/">frontend</a> and for the <a target="_blank" rel="noopener noreferrer" href="https://cloud.docker.com/repository/docker/kinast/counting-procedures-backend/tags/">backend</a></p>
        <p>If you don't want to use the command line, install <a target="_blank" rel="noopener noreferrer" href="https://kitematic.com/">Kitematic</a> for Docker to have a graphical user interface</p>
      </div>
      <div class="card">
        <h1>What the <a target="_blank" rel="noopener noreferrer" href="https://help.github.com/articles/fork-a-repo/">Fork?</a></h1>
        <p>If you want to contribute to the homepage you will have to <a target="_blank" rel="noopener noreferrer" href="https://help.github.com/articles/fork-a-repo/">fork</a> it first!</p>
        <p>If you need help you can contact <a href="mailto:countingprocedures@gmail.com">countingprocedures@gmail.com</a> but I'm supporting this in my freetime only, so don't expect a fast answer.</p>
        <p>Also if this homepage was forked and nobody changed this, I am maybe the wrong person to contact. The original homepage can be found on <a target="_blank" rel="noopener noreferrer" href="http://countingprocedures.com/">countingprocedures.com</a></p>
      </div>
      <div class="card">
        <h1>Get a copy of the Code</h1>
        <p>The code for the <a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org/kibsi/counting-procedures-frontend/">frontend</a> and for the <a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org/kibsi/counting-procedures-backend/src/master/">backend</a> can be found on Bitbucket</p>
        <p>In order to modify the homepage you will have to get the Code first</p>
        <p>There are many ways to get the Code, but I recomend to <a target="_blank" rel="noopener noreferrer" href="https://help.github.com/articles/fork-a-repo/">fork</a> the project first on Github or Bibucket and then check out the code using <a target="_blank" rel="noopener noreferrer" href="https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/">Git</a> </p>
        <p>Usually you can find the git clone command for the repositories on the git page!</p>
        <p>frontend: <code>git clone https://kibsi@bitbucket.org/kibsi/counting-procedures-frontend.git</code></p>
        <p>backend: <code>git clone https://kibsi@bitbucket.org/kibsi/counting-procedures-backend.git</code></p>
      </div>
      <div class="card">
        <h1>Run the Code in a Docker Container</h1>
        <p>If you don't want to set up all the development tools needed to continue developing you can just use the Docker Containers instead to run your code!</p>
        <p>To do that you will have to get your code into the container when you run it!</p>
        <p>You can use the -v option to mount your folder with the code as a VOLUME in docker. The target directory is /app in the Docker containers!</p>
        <p>Example: Your Code is located in the folder <code>C://frontend</code> on your Windows Computer</p>
        <p>Allow docker to acces your files first!!! Go to Settings/Shared Drives of your Docker installation and enable the Drives you want to access!</p>
        <p>Use <code>-v c:/frontend:/app</code></p>
        <p>I found a nice explanation of <a target="_blank" rel="noopener noreferrer" href="https://rominirani.com/docker-on-windows-mounting-host-directories-d96f3f056a2c">how to use Docker volumes</a></p>
        <p>If you run your container now, all the changes should be reflected into the container!</p>
      </div>
      <div class="card">
        <h1>Develop the frontend</h1>
        <p>The frontend was developed using the <a target="_blank" rel="noopener noreferrer" href="https://www.polymer-project.org/3.0/docs/devguide/feature-overview">Polymer Project</a></p>
        <p>If you want to develop the frontend and don't want to use the docker container to run the homepage, you will need at least the following steps:</p>
        <p> 1) <a target="_blank" rel="noopener noreferrer" href="https://www.guru99.com/download-install-node-js.html">Install Node and npm</a></p>
        <p> 2) <a target="_blank" rel="noopener noreferrer" href="https://www.npmjs.com/package/polymer-cli">Install polymer-cli: </a>Once you have installed npm, just run: <code>npm install -g polymer-cli</code></p>
        <p> 3) In your code folder run <code>npm install</code> to download and install all necessary dependencies</p>
        <p> 4) In your code folder run <code>polymer serve</code>(only your computer has access) or <code>polymer serve -H 0.0.0.0</code> (other computers in your network have access too) to start up the homepage! You will find the homepage served on <a target="_blank" rel="noopener noreferrer" href="localhost:8081">localhost:8081</a></p>
        <p> 5) I recomend to use some Code Editor like <a target="_blank" rel="noopener noreferrer" href="https://code.visualstudio.com">Visual Studio Code</a> (There are many more options)</p>
        <p> 6) Once you are done developing you maybe want to share your results. So upload it to your forked git repository and share the link!</p>
        <p> 7) You can also build a Docker container running <code>docker build -t YOUR_DOCKER_ACCOUNT/YOUR_FRONTEND_SERVER_NAME .</code> in your source folder! (Don't forget the ".")</p>
        <p> 8) Now you can upload your frontend to the docker cloud using <code>docker push YOUR_DOCKER_ACCOUNT/YOUR_FRONTEND_SERVER_NAME:latest</code> (You can use other tags than "latest")</p>
        <p> 9) Try running your container!!! Look at the Docker section above to understand the run command!</p>
        <p>10) Share the happy news! 👍</p>
      </div>
      <div class="card">
        <h1>Develop the backend</h1>
        <p>The backend is a stateless REST server</p>
        <p>The backend combines the following technologies: <a target="_blank" rel="noopener noreferrer" href="https://spring.io">Spring</a>, <a target="_blank" rel="noopener noreferrer" href="http://www.jython.org/">Jython</a>, JAVA</p>
        <p>If you want to develop the backend and don't want to use the docker container to run the homepage, you will need at least the following steps:</p>
        <p>Currently the port 8090 that is used by the frontend is hardcoded in the frontend, so you will have to use this port. It is planed to make that variable configurable in some settings file.</p>
        <p>You can easily change the port in the Code though!</p>
        <p> 1) Download and install the <a target="_blank" rel="noopener noreferrer" href="https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html">Java Development Kit (jdk)</a><p>
        <p> 2) Download and install <a target="_blank" rel="noopener noreferrer" href="https://maven.apache.org">Maven</a></p>
        <p> 3) Make sure you set the <a target="_blank" rel="noopener noreferrer" href="https://www.thewindowsclub.com/set-java_home-in-windows-10">JAVA_HOME</a> and <a target="_blank" rel="noopener noreferrer" href="https://www.mkyong.com/maven/how-to-install-maven-in-windows/">M2_HOME</a> Environment variables, especially if you use Windows</p>
        <p> 4) In your code folder run <code>mvn clean install dependency:copy-dependencies</code> to download all necessary dependencies (We still have problems with Jython in the generated jar file, so we run the unpackaged code instead)</p>
        <p> 5) In your code folder run <code>mvn exec:java</code> to start the REST server</p>
        <p> 6) I recomend to use some Code Editor like <a target="_blank" rel="noopener noreferrer" href="https://www.eclipse.org/downloads/">Eclipse</a>, <a target="_blank" rel="noopener noreferrer" href="https://netbeans.org/">Netbeans</a> or <a target="_blank" rel="noopener noreferrer" href="https://www.jetbrains.com/idea/download/">IntelliJ IDEA</a> (There are many more options) and configure Maven to work with your Editor</p>
        <p> 7) Once you are done developing you maybe want to share your results. So upload it to your forked git repository and share the link!</p>
        <p> 8) You can also build a Docker container running <code>docker build -t YOUR_DOCKER_ACCOUNT/YOUR_BACKEND_SERVER_NAME .</code> in your source folder! (Don't forget the ".")</p>
        <p> 9) Now you can upload your frontend to the docker cloud using <code>docker push YOUR_DOCKER_ACCOUNT/YOUR_BACKEND_SERVER_NAME:latest</code> (You can use other tags than latest)</p>
        <p>10) Try running your container!!! Look at the Docker section above to understand the run command!</p>
        <p>11) Share the happy news! 👍</p>
      </div>
    `;
  }
}

window.customElements.define('my-development-view', MyView2);
