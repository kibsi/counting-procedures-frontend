/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import {getMyGlobalStyle} from './shared-styles.js';
class MyFormatter {
    static styleStringForTitle(string_token){
        return MyFormatter._capitalize(MyFormatter._underscoresToSpaces(MyFormatter._dashToSpaces(MyFormatter._camelCaseToDash(string_token))));
    }

    static _capitalize(string_token) {
        return string_token.replace(/\w\S*/g, function(txt){
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    static _camelCaseToDash(string_token) {
        return !string_token ? null : string_token.replace(/([A-Z])/g, function (g) { return '-' + g[0].toLowerCase() });
            //return string_token.replace('/([A-Z])/g', (g) => -${g[0].toLowerCase()});
    }

    static _dashToSpaces(string_token){
        return string_token.split('-').join(' ');
    }

    static _underscoresToSpaces(string_token){
        return string_token.split('_').join(' ');
    }

    static padLeft(nr, n, str){
        //console.log("padding left");
        var nrLength = String(nr).length;
        if(nrLength>n){
          return nr;
        }
        return Array(n-nrLength+1).join(str||'0')+nr;
    }
    
    static padRight(nr, n, str){
        //console.log("padding left");
        var nrLength = String(nr).length;
        if(nrLength>n){
          return nr;
        }
        return nr+Array(n-nrLength+1).join(str||'0');
    }
}

class MyConversionHelper {
    static arrayToText(array,...headerArray){
        if(!array || array.length <1){
            return;
        }
        let containsArrays = Array.isArray(array[0]);
        if(containsArrays){
            var headerString="";
            if(headerArray && headerArray.length>0){
                headerString = headerArray.join(";")+"\n";
            }
            return headerString+array.map(function(v){
                return v.join(";")+"\n";
            }).join("");
        }else{
            var firstElement = array[0];
            var keys = Object.keys(firstElement);
            console.log({keys,firstElement});
            var header = keys.join(";")+";\n";
            console.log({header});
            var body = array.map(row=>keys.map(key=>row[key]).join(";")+";\n").join("");
            console.log({body});
            return header.concat(body);
        }
    }
}

class DownloadHelper {

    static saveText(textToSave){
        if(!(textToSave instanceof String)){
            console.error("Can not save unsupported object: ",{textToSave});
            return;
        }
        console.log("Saving text to txt: ",{textToSave});
        var a = document.createElement('a');
        a.href = "data:text/plain;base64,"+btoa(textToSave);
        a.download = "result.txt";  
        a.click();
        a.remove();
    }

    static saveCsv(variableToSave){
        if(variableToSave.push){
            var textToSave = "";
            variableToSave.forEach(function(row){
                if(Array.isArray(row)){
                    textToSave += row.join(";")+"\n";
                }else{
                    textToSave += row+";\n";
                }
            });
            console.log("saving Array to csv: ",{textToSave});
        }else if(variableToSave.substr){
            var textToSave = variableToSave;
            console.log("saving String to csv: ",{textToSave});
        }else{
            console.error("Can not save unsupported object: ",{variableToSave});
            return;
        }
        console.log({textToSave});
        var a = document.createElement('a');
        a.href = "data:text/csv;base64,"+btoa(textToSave);
        a.download = "result.csv";  
        a.click();
        a.remove();
    }

    static saveSVG(svgToSave){

        svgToSave.setAttribute("title","result");
        svgToSave.setAttribute("version","1.1");
        svgToSave.setAttribute("xmlns","http://www.w3.org/2000/svg");

        var htmlToDownload = svgToSave.outerHTML;
        console.log({htmlToDownload});
        var a = document.createElement('a');
        a.href = "data:image/svg+xml;base64,"+btoa(htmlToDownload);
        a.download = "result.svg";  
        a.click();
        a.remove();
    }

    static wrapHtml(partToSave){

        var positionInfo = partToSave.getBoundingClientRect();
        var width = positionInfo.width;
        var height = positionInfo.height;
        if(!width){
            width = 800;
            console.log("Width was invalid, new width=",{width});
        }
        if(!height){
            height = 600;
            console.log("height was invalid, new height=",{height});
        }
        var htmlToDownload = "<svg xmlns='http://www.w3.org/2000/svg' title='result' width='"+width+"' height='"+height+"' version='1.1'>" +
        "<style>" + 
        getMyGlobalStyle() +
        "</style>" +         
        "<foreignObject class='node' width='100%' height='100%'>" + 
        "<body xmlns='http://www.w3.org/1999/xhtml'>" +
        partToSave.outerHTML +
        "</body>" +
        "</foreignObject>" +
        "</svg>";
        console.log({htmlToDownload});
        return htmlToDownload;
    }

    static saveHtmlAsSVG(partToSave){
        var htmlToDownload = DownloadHelper.wrapHtml(partToSave);
        var a = document.createElement('a');
        a.href = "data:image/svg+xml;base64,"+btoa(htmlToDownload);
        a.download = "result.svg";  
        a.click();
        a.remove();
    }

    static saveHtmlAsPng(partToSave){
        var svgToSave = DownloadHelper.wrapHtml(partToSave);
        console.log({svgToSave});
        var xml = svgToSave;
        var svg64 = btoa(xml);
        var b64Start = "data:image/svg+xml;base64,";
        var image64 = b64Start + svg64;
        var myImage = document.createElement("img");
        var canvas = document.createElement("canvas");
        var positionInfo = partToSave.getBoundingClientRect();
        var width = positionInfo.width;
        var height = positionInfo.height;
        console.log({width,height,myImage})
        canvas.setAttribute("width",width);
        canvas.setAttribute("height",height);
        console.log({canvas,image64});
        myImage.src = image64;
        myImage.onload = function() {
            var context = canvas.getContext("2d");
            context.drawImage(myImage,0,0);
            var a = document.createElement("a");
            a.download = "result.png";
            a.href = canvas.toDataURL("image/png");
            a.click();
            a.remove();
            console.log("started download PNG");
        }
        myImage.remove();
        canvas.remove();
    }

    static saveSvgAsPng(svgToSave){

        console.log({svgToSave});
        var xml = new XMLSerializer().serializeToString(svgToSave);
        var svg64 = btoa(xml);
        var b64Start = "data:image/svg+xml;base64,";
        var image64 = b64Start + svg64;
        var myImage = document.createElement("img");
        var canvas = document.createElement("canvas");
        var width = svgToSave.getAttribute("width");
        var height = svgToSave.getAttribute("height");
        console.log({width,height,myImage});
        canvas.setAttribute("width",width);
        canvas.setAttribute("height",height);
        console.log({myImage,image64});
        myImage.src = image64;
        myImage.onload = function() {
            var context = canvas.getContext("2d");
            context.drawImage(myImage,0,0);
            var a = document.createElement("a");
            a.download = "result.png";
            a.href = canvas.toDataURL("image/png");
            a.click();
            a.remove();
            console.log("started download PNG");
        }
        myImage.remove();
        canvas.remove();
    }
}

export {MyFormatter, DownloadHelper,MyConversionHelper};