/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import * as d3 from "d3";
import {DownloadHelper} from './my-helpers';


class MyHorizontalBarChart extends PolymerElement {
    static get template() {
      return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
      </style>
        <div id="my-containing-div"><svg id="barChart"></svg></div>
        <div>
          <paper-button id="saveSVG" class="blueish">SVG<iron-icon icon="file-download"/></paper-button>
          <paper-button id="savePNG" class="blueish">PNG<iron-icon icon="file-download"/></paper-button>
        </div>
        <img id="conversion-img" width="[[width]]" height="[[height]]" hidden/>
        <canvas id="conversion-canvas" width="[[width]]" height="[[height]]" hidden></canvas>
      `;
    }

    constructor(){
      super();
      window.addEventListener('resize', (e) => {this._handleResize()});
    }

    static get properties() {
        return {
            width: {
                type: String,
                value: "-1",
                notify: true,
                //reflectToAttribute:true,
            },
            height: {
                type: String,
                value: "-1",
                notify: true,
                //reflectToAttribute:true,
            },
            tableData: {
                type: Array,
                //value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            tableDataObject:{
                type: Object,
                notify: true,
            },
        };
    }

    static get observers(){
        return [
            '_tableDataToObject(tableData)',
            '_refreshChart(tableDataObject)',
            '_refreshChart(tableData)',
            '_refreshChart(width)',
            '_refreshChart(height)',
        ];
    }
    ready(){
      super.ready();
      console.log("barChart: ready");
      this.set("tableData",[]);
    }

    _handleResize(){
      console.log("resize");
      this._refreshChart("resize");
    }

    _tableDataToObject(tableData){

      var tableDataObject = {};
      if(!tableData || tableData.length<1){
        return;
      }
      console.log({tableData});
      var lastIndex = tableData[0].length-1;
      if(lastIndex < 1){
        return;
      }
      var titles = tableData.map(function(v){return v[0];});
      var infos = tableData.map((v,i) => v.slice(0,lastIndex));
      console.log({infos});

      var values = tableData.map(function(v){return parseFloat(v[lastIndex]);});

      tableDataObject.titles=titles;
      tableDataObject.infos=infos;
      tableDataObject.values=values;
      console.log({tableDataObject});
      this.set("tableDataObject",tableDataObject);
    }
    
      _refreshChart(changeRecord){
        var tableDataObject = this.get("tableDataObject");
        if(!tableDataObject){
          console.log("No tableDataObject found in _refreshChart");
          return;
        }
        //console.log("barChart: _refreshChart");
        //console.log(tableDataObject);
        var titles = tableDataObject.titles;
        var infos = tableDataObject.infos;
        var values = tableDataObject.values;
        var containingDiv = this.shadowRoot.querySelector("#my-containing-div");
        var shadowSvg = this.shadowRoot.querySelector("#barChart");
        var barWidth = parseFloat(this.get("width"));
        var barHeight = parseFloat(this.get("height"));
        var margin = {top: 20, right:20, bottom:30, left:100};
        if(barWidth<0){
          barWidth = containingDiv.offsetWidth;
        }
        if(barWidth<margin.left + margin.right){
          console.log("using window width!");
          barWidth = (window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth)-110;
        }
        if(barHeight<0){
          barHeight = containingDiv.offsetHeight;
        }
        if(barHeight<margin.top + margin.bottom){
          barHeight = 800;
        }
        shadowSvg.setAttribute("width",barWidth);
        shadowSvg.setAttribute("height",barHeight);
        var svg = d3.select(shadowSvg);
        // delete old content
        svg.html(null);
        var width = barWidth - margin.left - margin.right;
        var height = barHeight -margin.top - margin.bottom;
        var g = svg.attr("title","result")
          .attr("version","1.1")
          .attr("xmlns","http://www.w3.org/2000/svg")
          .append("g").attr("transform","translate("+margin.left+","+margin.top+")");
        
        var barSpace = (height/values.length)*0.1;
        var barHeight = (height/values.length)-barSpace;

        var x = d3.scaleLinear().range([0,width]);
        var y = d3.scaleLinear().range([height,0]);
        var colors = d3.scaleOrdinal(d3.schemeCategory10);
        
        var xDomain = [0,d3.max(values)];
        //console.log("xDomain: %s",JSON.stringify(xDomain));
        x.domain(xDomain);
        var yDomain = [0,values.length];
        //console.log("yDomain: %s",JSON.stringify(yDomain));
        y.domain(yDomain);
        
        var shadowYTickTooltip = this.shadowRoot.querySelector("#my-y-tick-tooltip");

        var grid = d3.range(25).map(function(d,i){
          return {"x1":i*30,"y1":0,"x2":i*30,"y2":height};
        });

        
          g.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x)
              .tickSizeInner(-height)
          ).append("text")
            .attr("dx", "50.00em")
            .attr("dy", "3.00em")
            .attr("fill", "#000")
            .text("N");
            
          var insertLineBreaks = function(d) {
            var el = d3.select(this);
            var lines = d3.select(this).text().splitt("\n");
            el.text(null);
            for(var line in lines){
              var tspan = el.append("tspan").text(line);
              tspan.attr("x",0).attr("dy","1.2em");
            }
          }

            console.log({infos});
            var yAxis = g.append("g")
            .attr("class", "axis axis--y")
            .call(d3.axisLeft(y)
            .tickValues(d3.range(titles.length)))
            .selectAll("text")
            .text("")
            .selectAll("tspan")
            .data((d,i) => {
              let infoElement = infos[i].slice().reverse();
              //console.log({infoElement});
              return infoElement;
            })
            .enter()
            .append("tspan")
            .text(d => d)
            .attr("x","0")
            .attr("dx","-1em")
            .attr("dy",(d,i)=>-1+"em");
        
        var chart = g.append("g")
          .attr("id","bars")
          .selectAll("rect")
          .data(values)
          .enter()
          .append("rect")
          .attr("x","0")
          .attr("y",function(v,i){return y(i)-barHeight-barSpace/2;})
          .attr("width",function(v){return x(v);})
          .attr("height",barHeight)
          .style("fill",function(v,i){return colors(i);})
          .style("stroke","black")
          .style("stroke-width","1px");
          

        var chartValues = g.append("g")
          .attr("id","chartValues")
          .selectAll("text")
          .data(values)
          .enter()
          .append("text")
          .attr("x",function(d) {
            return d3.max([10,x(d)-100]);
            //return 100;
          })
          .attr("y",function(d,i){ return y(i)-barHeight/2-barSpace/2+6; })
          .text(function(d){ return d; })
          .style("fill","black")
          .style("font-size","14px");

          var saveSVGButton = this.shadowRoot.querySelector("#saveSVG");
          d3.select(saveSVGButton)
            .on("click", writeSVGDownloadLink);

          var savePNGButton = this.shadowRoot.querySelector("#savePNG");
          d3.select(savePNGButton)
            .on("click", writePNGDownloadLink);

          function writeSVGDownloadLink(){
            DownloadHelper.saveSVG(shadowSvg);
          }
          
          function writePNGDownloadLink(){
            DownloadHelper.saveSvgAsPng(shadowSvg);
          }
      }
}

window.customElements.define('my-horizontal-bar-chart', MyHorizontalBarChart);