/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-input/paper-input.js';
//import './my-horizontal-bar-chart.js';
import './my-simple-table.js';
import './my-xy-chart.js';
//import './my-range-count-result.js';
//import './my-class-crossings-result.js';
import {MyFormatter} from './my-helpers.js';
import {MyResultConverter} from './my-result-converter.js';

class MyMarkovResult extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
                padding: 10px;
            }
        </style>
        <template is="dom-if" if="[[_exists(dataObject.markovProbabilities)]]">
            <div class="card">
                <h2>Übergangswahrscheinlichkeiten</h2>
                <paper-tabs selected="{{selectedIndexMarkovProbabilities}}">
                    <paper-tab>Table</paper-tab>
                    <paper-tab>Text</paper-tab>
                </paper-tabs>
                <iron-pages selected="[[selectedIndexMarkovProbabilities]]">
                    <my-simple-table table-data="[[_limitPrecision(dataObject.markovProbabilities,precision)]]" table-header="[[_calculateClassHeaders(dataObject.dataClasses)]]"></my-simple-table>
                    <paper-textarea value="[[dataObject.results.markovProbabilitiesString]]" max-rows="9" readonly></paper-textarea>
                </iron-pages>
                <paper-input label="precision" value="{{precision}}"></paper-input>
                <div>
                    <paper-input label="N" value="{{markovN}}" allowed-pattern="[0-9]*"></paper-input>
                    <paper-button class="blueish" on-click="_generateFromMarkovProbabilities">generate BZF</paper-button>
                </div>
                <template is="dom-if" if="[[_exists(dataObject.results.markovGeneratedObject)]]">
                    <div class="card">
                        <my-xy-chart data-object="[[dataObject.results.markovGeneratedObject]]" height="300"></my-xy-chart>
                    </div>
                </template>
            </div>
        </template>
        `;
    }

    static get properties() {
        return {
            dataObject:{
                type: Object,
                value: {},
                notify: true,
            },
            precision: {
                type: Number,
                value: 3,
                notify: true,
            },
            markovProbabilities: {
                type: Array,
                notify: true,
                value:[],
                reflectToAttribute:true,
            },
            calculationClasses:{
                type: Array,
                notify: true,
                reflectToAttribute:true,
            },
            yLines:{
                type: Array,
                notify: true,
                reflectToAttribute:true,
            },
            classHeaders: {
                type: Array,
                notify: true,
            },
            selectedIndexMarkovProbabilities: {
                type: String,
                value: "0",
                notify: true,
                reflectToAttribute:true,
            },
            markovN: {
                type: Number,
                value: 100,
                notify: true,
                reflectToAttribute:true,
            },
        }
    }

    static get observers(){
        return [
            '_markovProbabilitiesToString(dataObject.markovProbabilities)',
        ];
    }

    _exists(target){
        return target ? true : false;
    }

    _calculateClassHeaders(dataClasses){
        return MyResultConverter.objectsToArray(dataClasses,["index","max","min"]);
    }

    _limitPrecision(table,precision){
        if(table){
            if(isNaN(precision)||precision<1||precision>99){
                return table;
            }
            return table.map(row=>row.map(cell=>cell==0?cell:Number.parseFloat(cell).toPrecision(precision)));
        }
    }
    
    _isEmpty(array,changeRecord){
        if(array==undefined){
          //console.log("_isEmpty undefined - true");
          return true;
        }
        if(array.length>0){
          //console.log("_isEmpty false");
          return false;
        }
        //console.log("_isEmpty true");
        return true;
    }
    
    _computeCalculationClassHeight(changeRecord){
        //console.log("_computeCalculationClassHeight");
        //console.log(changeRecord.base);
        if(changeRecord.base.length==0){
          return 0;
        }
        return ((changeRecord.base.length)*10)+500;
    }

    _markovProbabilitiesToString(markovProbabilities){
        console.log({markovProbabilities});
        if(markovProbabilities==undefined || markovProbabilities.length==0){
            return;
        }
        //var decimals = 3;
        //var multiplier = Math.pow(10,decimals); 
        var header = Array.apply(null, {length: markovProbabilities.length}).map(Number.call, Number);
        console.log(header);
        header = header.map(function(v,i){return MyFormatter.padRight(MyFormatter.padLeft(v+1,3,"_"),4,"_");});
        console.log(header);
        //var result = "        "+header.join(".;")+"\n";
        var result = "";
        for(var i=0;i<markovProbabilities.length;i++){
            //result += MyFormatter.padLeft(i+1,5," ")+": ";
            for(var j=0;j<markovProbabilities.length;j++){
                //var value = ""+Math.round(markovProbabilities[i][j]*multiplier)/multiplier;
                var value = markovProbabilities[i][j];
                //if(!value.includes(".")){
                //    value += ".";
                //}
                //result += MyFormatter.padRight(value,decimals+2,"0") + ";"
                result += value+";";
            }
            result += "\n";
        }
        var markovProbabilitiesString = result;
        console.log({markovProbabilitiesString});
        

        this.set("dataObject.results.markovProbabilitiesString",markovProbabilitiesString);
    }

    _generateFromMarkovProbabilities(){
        var N = this.get("markovN");
        var dataObject = this.get("dataObject");
        var probabilities = dataObject.markovProbabilities;
        var dataClasses = dataObject.dataClasses;
        var markovGeneratedValues = [];
        markovGeneratedValues.length = N;
        var col=0;
        var row=0;
        var nextRand = 1.0;
        var minRoundingError = 0.000001;
        outer: for(row = 0; row<probabilities.length;row++){
            for(col = 0; row<probabilities[row].length;col++){
                if(probabilities[row][col]>0){
                    break outer;
                }
            }
        }
        for(var i = 0;i<N;i++){
            nextRand = Math.random();
            console.log("nextRand is:"+nextRand);
            console.log(probabilities[row]);
            for(col = 0; col < probabilities[row].length;col++){
                nextRand=nextRand-probabilities[row][col];
                if(nextRand<=minRoundingError){
                    row=col;
                    console.log("chose index: "+col);
                    var y=(dataClasses[col].min+dataClasses[col].max)/2;
                    var x = i;
                    markovGeneratedValues[i] = {x,y};
                    break;
                }
            }
        }
        var markovGeneratedObject = {};
        markovGeneratedObject.dataClasses = dataClasses;
        markovGeneratedObject.dataPoints = markovGeneratedValues;
        let lines = markovGeneratedObject.dataPoints.slice(0,markovGeneratedObject.dataPoints.length-1);
        lines = lines.map((v,i)=>{return{x1:v.x,y1:v.y,x2:markovGeneratedObject.dataPoints[i+1].x,y2:markovGeneratedObject.dataPoints[i+1].y}});
        markovGeneratedObject.lines = lines;
        markovGeneratedObject.yLines = dataObject.yLines;
        console.log({markovGeneratedValues});
        this.set("dataObject.results.markovGeneratedObject",markovGeneratedObject);
    }
}

window.customElements.define('my-markov-result', MyMarkovResult);