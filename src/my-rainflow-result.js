/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import 'paper-collapse-item/paper-collapse-item.js';
import './my-horizontal-bar-chart.js';
import './my-class-crossings-evaluation.js';
import {MyResultConverter} from './my-result-converter.js';

class MyRainflowResult extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
            }
        </style>
        <!-- div id="containingdiv" width="{{containingWidth}}" -->
            <template is="dom-if" if="[[_exists(dataObject.gridFromTo)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Vollmatrix</h2>
                        <paper-tabs selected="{{selectedTabIndexFromToGrid}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexFromToGrid]]">
                            <my-simple-table table-data="[[dataObject.gridFromTo]]" table-header="[[_calculateClassHeaders(dataObject.dataClasses)]]"></my-simple-table>
                            <paper-textarea value="[[_ArrayToText(dataObject.gridFromTo)]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div>
            </template>
            <template is="dom-if" if="[[_exists(dataObject.gridFromTo)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Halbmatrix (aus Vollmatrix)</h2>
                        <paper-tabs selected="{{selectedTabIndexFromToGrid}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexFromToGrid]]">
                            <my-simple-table table-data="[[dataObject.results.halfMatrix]]" table-header="[[_calculateClassHeaders(dataObject.dataClasses)]]"></my-simple-table>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.halfMatrix)]]"></paper-textarea>
                        </iron-pages>
                        <div>
                            <paper-input label="Faktor Residuum" value="{{residuumFactor}}"></paper-input>
                            <p>Unregelmäßigkeitsfaktor: [[dataObject.results.irregularity]]</p>
                        </div>
                    </paper-collapse-item>
                </div>
            </template>
            <template is="dom-if" if="[[_exists(dataObject.results.peaksLabeled)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Spitzenzählung (aus Halbmatrix)</h2>
                        <paper-tabs selected="{{selectedTabIndexClassCrossings}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexClassCrossings]]">
                            <my-horizontal-bar-chart table-data="[[dataObject.results.peaksLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.peaksLabeled)]]"></my-horizontal-bar-chart>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.peaks,'name','max','min','amount')]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div>
            </template>
            <template is="dom-if" if="[[_exists(dataObject.results.classCrossingsLabeled)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Klassengrenzenüberschreitungszählung (aus Halbmatrix)</h2>
                        <paper-tabs selected="{{selectedTabIndexClassCrossings}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexClassCrossings]]">
                            <my-horizontal-bar-chart table-data="[[dataObject.results.classCrossingsLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.peaksLabeled)]]"></my-horizontal-bar-chart>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.classCrossings)]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div>
            </template>
            <!-- template is="dom-if" if="[[_exists(dataObject.results.accumulatedCyclesLabeled)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Summenhäufigkeit (Abgeleitet)</h2>
                        <paper-tabs selected="{{selectedTabIndexAccumulatedCycles}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexAccumulatedCycles]]">
                                <my-horizontal-bar-chart table-data="[[dataObject.results.accumulatedCyclesLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.accumulatedCyclesLabeled)]]"></my-horizontal-bar-chart>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.accumulatedCycles,'name','max','min','amount')]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div -->
            <template is="dom-if" if="[[_exists(dataObject.results.amplitudesLabeled)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Spannungsamplituden (aus Halbmatrix)</h2>
                        <paper-tabs selected="{{selectedTabIndexTensionAmplitudes}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexTensionAmplitudes]]">
                            <my-horizontal-bar-chart table-data="[[dataObject.results.amplitudesLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.amplitudesLabeled)]]"></my-horizontal-bar-chart>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.amplitudes,'name','amplitude','min','amount')]]"></paper-textarea>
                        </iron-pages>
                    </paper-collapse-item>
                </div>
            </template>
            <template is="dom-if" if="[[_exists(dataObject.results.derivedProbabilities)]]">
                <div class="card">
                    <paper-collapse-item>
                        <h2 slot="header">Wahrscheinlichkeiten (aus Vollmatrix)</h2>
                        <paper-tabs selected="{{selectedTabIndexDerivedProb}}" scrollable>
                            <paper-tab>Chart</paper-tab>
                            <paper-tab>CSV</paper-tab>
                        </paper-tabs>
                        <iron-pages selected="[[selectedTabIndexDerivedProb]]">
                            <my-simple-table table-data="[[_limitPrecision(dataObject.results.derivedProbabilities,precision)]]" table-header="[[_calculateClassHeaders(dataObject.dataClasses)]]"></my-simple-table>
                            <paper-textarea value="[[_ArrayToText(dataObject.results.derivedProbabilities)]]"></paper-textarea>
                        </iron-pages>
                        <paper-input label="precision" value="{{precision}}"></paper-input>
                    </paper-collapse-item>
                </div>
            </template>
            <div class="card">
                <paper-collapse-item>
                    <h2 slot="header">Generiere Werte (aus Wahrscheinlichkeiten)</h2>
                    <paper-input label="N" value="{{pointsToGenerateN}}" allowed-pattern="[0-9]*"></paper-input>
                    <!-- paper-button class="blueish" on-click="_generateFromMarkovProbabilities">generate BZF</paper-button -->
                    <template is="dom-if" if="[[_exists(dataObject.results.generatedDataPoints)]]">
                        <my-xy-chart data-object="[[dataObject.results.generatedDataPoints]]" height="300"></my-xy-chart>
                    </template>
                </paper-collapse-item>
            </div>
            <template is="dom-if" if="[[_exists(dataObject.results.accumulatedAmplitudesLabeled)]]">
                <my-class-crossings-evaluation class-crossings-result-labeled="[[dataObject.results.accumulatedAmplitudesLabeled]]" class-crossings-result="[[dataObject.results.accumulatedAmplitudes]]"></my-class-crossings-evaluation>
            </template>
        <!-- /div -->
        `;
    }

    static get properties() {
        return {
            dataObject:{
                type: Object,
                value: {},
                notify: true,
            },
            residuumFactor: {
                type: Number,
                value: 0,
            },
            precision: {
                type: Number,
                value: 3,
                notify: true,
            },
            pointsToGenerateN: {
                type: Number,
                value: 200,
            },
            selectedTabIndexClassCrossings:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexAccumulatedCycles:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexTensionAmplitudes:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexFromToGrid:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexDerivedProb:{
                type: Number,
                value: 0,
                notify: true,
            },
        }
    }

    static get observers(){
        return [
            '_initDataObject(dataObject)',
            '_computeHalveMatrix(dataObject.gridFromTo,residuumFactor,"dataObject.results.halfMatrix")',
            '_computePeaks(dataObject.dataClasses,dataObject.results.halfMatrix,"dataObject.results.peaks")',
            '_computePeaksLabeled(dataObject.dataClasses,dataObject.results.halfMatrix,"dataObject.results.peaksLabeled")',
            '_computeClassCrossingsLabeled(dataObject.dataClasses,dataObject.results.halfMatrix,"dataObject.results.classCrossingsLabeled")',
            '_computeClassCrossings(dataObject.dataClasses,dataObject.results.halfMatrix,"dataObject.results.classCrossings")',
            '_extractAmplitudes(dataObject.dataClasses,dataObject.results.halfMatrix,"dataObject.results.amplitudes")',
            '_extractAmplitudesLabeled(dataObject.dataClasses,dataObject.results.halfMatrix,"dataObject.results.amplitudesLabeled")',
            '_calculateIrregularity(dataObject.results.halfMatrix,"dataObject.results.irregularity")',
            //'_computeAccumulatedCycles(dataObject.dataClasses)',
            //'_computeAccumulatedCyclesLabeled(dataObject.dataClasses)',
            //'_computeTensionAmplitudes(dataObject.dataClasses)',
            //'_computeTensionAmplitudesLabeled(dataObject.dataClasses)',
            '_calculateProbabilities(dataObject.gridFromTo,"dataObject.results.derivedProbabilities")',
            '_generateFromProbabilities(dataObject.dataClasses,dataObject.results.derivedProbabilities,pointsToGenerateN,dataObject.yLines,"dataObject.results.generatedDataPoints")',
        ];
    }

    
    _computeHalveMatrix(gridFromTo,factor,resultName){
        if(gridFromTo){
            console.log("_computeHalveMatrix");
            if(!factor){
                factor = 0;
            }
            var halfMatrix = gridFromTo.map((row,i)=>{
                return row.slice(0,i+1).map((cell,j)=>{
                    return Math.min(cell,gridFromTo[j][i])+Math.abs((cell-gridFromTo[j][i])*factor);
                });
            })
            this.set(resultName,halfMatrix);
        }
    }

    _computePeaks(dataClasses,halfMatrix,resultName){
        if(dataClasses&&halfMatrix){
            console.log("_computePeaks");
            var peaks = dataClasses.map(entry=>{
                var peaksForClass = halfMatrix[entry.index].slice(0,entry.index+1).reduce((prevVal,curVal)=>prevVal+curVal,0);
                return {name:entry.name,max:entry.max,min:entry.min,amount:peaksForClass};
            });
            console.log({peaks});
            this.set(resultName,peaks);
        }
    }

    _computePeaksLabeled(dataClasses,halfMatrix,resultName){
        if(dataClasses&&halfMatrix){
            console.log("_computePeaksLabeled");
            var peaksLabeled = dataClasses.map(entry=>{
                var peaksForClass = halfMatrix[entry.index].slice(0,entry.index+1).reduce((prevVal,curVal)=>prevVal+curVal,0);
                return [entry.name,entry.maxLabel,entry.minLabel,peaksForClass];
            });
            console.log({peaksLabeled});
            this.set(resultName,peaksLabeled);
        }
    }

    _computeClassCrossings(dataClasses,halfMatrix,resultName){
        if(dataClasses&&halfMatrix){
            console.log("_computeClassCrossings");
            var classCrossings = dataClasses.map(entry=>{
                var currentClass = entry.index;
                var crossingsOfClass = this._extractClassCrossingsForIndex(currentClass,halfMatrix);
                return {name:entry.name,max:entry.max,min:entry.min,amount:crossingsOfClass};
            });
            console.log("computed: ",{classCrossings});
            this.set(resultName,classCrossings);
        }
    }

    _computeClassCrossingsLabeled(dataClasses,halfMatrix,resultName){
        if(dataClasses&&halfMatrix){
            console.log("_computeClassCrossingsLabeled");
            var classCrossingsLabeled = dataClasses.map(entry=>{
                var currentClass = entry.index;
                var crossingsOfClass = this._extractClassCrossingsForIndex(currentClass,halfMatrix);
                return [entry.name,entry.maxLabel,entry.minLabel,crossingsOfClass];
            });
            console.log("computed: ",{classCrossingsLabeled});
            this.set(resultName,classCrossingsLabeled);
        }
    }

    _extractAmplitudes(dataClasses,halfMatrix,resultName){
        if(dataClasses&&halfMatrix){
            console.log("_extractAmplitudes");
            var size = halfMatrix.length;
            var amplitudes = dataClasses.map(entry=>{
                var currentClass = entry.index;
                var amplitudeAmount = halfMatrix.slice(currentClass,size).map((v,i)=>v[i]).reduce((prevVal,curVal)=>prevVal+curVal);
                return {name:entry.name,max:entry.max,min:entry.min,amount:amplitudeAmount};
            });
            console.log(resultName,{amplitudes});
            this.set(resultName,amplitudes)
        }
    }

    _extractAmplitudesLabeled(dataClasses,halfMatrix,resultName){
        if(dataClasses&&halfMatrix){
            console.log("_extractAmplitudes");
            var size = halfMatrix.length;
            var amplitudesLabeled = dataClasses.map(entry=>{
                var currentClass = entry.index;
                var amplitudeAmount = halfMatrix.slice(currentClass,size).map((v,i)=>v[i]).reduce((prevVal,curVal)=>prevVal+curVal);
                return [entry.name,entry.maxLabel,entry.minLabel,amplitudeAmount];
            });
            console.log(resultName,{amplitudesLabeled});
            this.set(resultName,amplitudesLabeled);
        }
    }

    _extractClassCrossingsForIndex(currentClass,halfMatrix){
        if(halfMatrix){
            var gridSize = halfMatrix.length;
            return halfMatrix.slice(currentClass,gridSize+1).map(line=>{
                var slicedLine = line.slice(0,currentClass+1);
                return slicedLine.reduce((previousValue,currentValue)=>previousValue+currentValue,0);
            }).reduce((previousValue,currentValue)=>previousValue+currentValue,0);
        }
    }

    _calculateIrregularity(halfMatrix,resultName){
        if(halfMatrix){
            var size = halfMatrix.length;
            var flooredHalfLength = Math.floor(size/2.0);
            var ceiledHalfLength = Math.ceil(size/2.0);
            var totalSum = halfMatrix.map(row=>row.reduce((prevVal,curVal)=>prevVal+curVal)).reduce((prevVal,curVal)=>prevVal+curVal);
            //var sliced = halfMatrix.slice(ceiledHalfLength,size).map(row=>row.slice(0,ceiledHalfLength));
            //console.log("sliced: ",{sliced});
            var sumOfCeiledSquare = halfMatrix.slice(ceiledHalfLength,size).map(row=>row.slice(0,ceiledHalfLength).reduce((prevVal,curVal)=>prevVal+curVal)).reduce((prevVal,curVal)=>prevVal+curVal);
            var sumOfSquare  = sumOfCeiledSquare;
            if(ceiledHalfLength - flooredHalfLength > 0){
                var sumOfFlooredSquare = halfMatrix.slice(flooredHalfLength,size).map(row=>row.slice(0,flooredHalfLength).reduce((prevVal,curVal)=>prevVal+curVal)).reduce((prevVal,curVal)=>prevVal+curVal);
                console.log("odd number of indexes, need to calculate second square for the difference!",{sumOfFlooredSquare})
                var borderValue = (sumOfCeiledSquare-sumOfFlooredSquare)/2.0;
                sumOfSquare = sumOfSquare - borderValue;
            }
            console.log("sums:",{totalSum,sumOfSquare});
            if(sumOfSquare>0){
                var irregularity = totalSum/(2*sumOfSquare);
            }else{
                var irregularity = -1;
            }
            this.set(resultName,irregularity);
        }
    }

    _calculateProbabilities(gridFromTo,resultName){
        if(gridFromTo){
            console.log("_calculateProbabilities");
            var probabilities = gridFromTo.map(row=>{
                var sum = row.reduce((cv,pv)=>cv+pv,0);
                if(sum>0){
                    return row.map(k=>k/sum);
                }
                return row;
            })
            this.set(resultName,probabilities);
        }
    }

    _generateFromProbabilities(dataClasses,probabilities,pointsToGenerate,yLines,resultName){
        if(dataClasses&&probabilities&&pointsToGenerate&&yLines){
            console.log("_generateFromProbabilities");
            var N = pointsToGenerate;
            var generatedValues = [];
            generatedValues.length = N;
            var col=0;
            var row=0;
            var nextRand = 1.0;
            var minRoundingError = 0.000001;
            console.log("probabilities:",{probabilities});
            console.log("search for start:");
            outer: for(row = 0; row<probabilities.length;row++){
                for(col = 0; col<probabilities[row].length;col++){
                    console.log("probs: ",row,col,probabilities[row][col]);
                    if(probabilities[row][col]>0){
                        break outer;
                    }
                }
            }
            console.log("starting!")
            for(var i = 0;i<N;i++){
                nextRand = Math.random();
                console.log("index:",{i,nextRand})
                //console.log("nextRand is:"+nextRand);
                //console.log(probabilities[row]);
                for(col = 0; col < probabilities[row].length;col++){
                    nextRand=nextRand-probabilities[row][col];
                    if(nextRand<=minRoundingError){
                        row=col;
                        //console.log("chose index: "+col);
                        var y=(dataClasses[col].min+dataClasses[col].max)/2;
                        var x = i;
                        generatedValues[i] = {x,y};
                        break;
                    }
                }
            }
            var generatedDataPoints = {};
            generatedDataPoints.dataClasses = dataClasses;
            generatedDataPoints.dataPoints = generatedValues;
            let lines = generatedDataPoints.dataPoints.slice(0,generatedDataPoints.dataPoints.length-1);
            lines = lines.map((v,i)=>{return{x1:v.x,y1:v.y,x2:generatedDataPoints.dataPoints[i+1].x,y2:generatedDataPoints.dataPoints[i+1].y}});
            generatedDataPoints.lines = lines;
            generatedDataPoints.yLines = yLines;
            console.log({generatedValues});
            this.set(resultName,generatedDataPoints);
        }
    }

    _exists(target){
        return target ? true : false;
    }

    _calculateClassHeaders(dataClasses){
        if(dataClasses){
            return MyResultConverter.objectsToArray(dataClasses,["viewIndex","max","min"]);
        }
    }

    _limitPrecision(table,precision){
        if(table){
            if(isNaN(precision)||precision<1||precision>99){
                return table;
            }
            return table.map(row=>row.map(cell=>cell==0?cell:Number.parseFloat(cell).toPrecision(precision)));
        }
    }

    

    

    _initDataObject(dataObject){
        if(dataObject&&!dataObject.results){
            dataObject.results={};
        }
    }
    
    _computeCalculationClassHeight(table,changeRecord){
        //console.log("_computeCalculationClassHeight");
        //console.log(changeRecord.base);
        if(changeRecord){
            var base = changeRecord.base;
        }else{
            var base = table;
        }
        if(!base || base.length==0){
          return 0;
        }
        var baseLength = base.length;
        console.log("Tabel and length",{base,baseLength});
        return ((base.length)*36)+100;
    }

    /*_computeAccumulatedCycles(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedCycles = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                return [v.name,v.max,v.min,accumulatedAmount];
            });
            console.log("computed: ",{accumulatedCycles});
            this.set("dataObject.results.accumulatedCycles",accumulatedCycles);
        }
    }
    _computeAccumulatedCyclesLabeled(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedCyclesLabeled = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                return [v.name,v.minLabel,v.maxLabel,accumulatedAmount];
            });
            console.log("computed: ",{accumulatedCyclesLabeled});
            this.set("dataObject.results.accumulatedCyclesLabeled",accumulatedCyclesLabeled);
        }
    }

    _computeTensionAmplitudes(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedAmplitudes = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                let amplitude = Math.abs(v.max/2);
                return {name:v.name,max:amplitude,min:0,amount:accumulatedAmount};
            });
            console.log("computed: ",{accumulatedAmplitudes});
            this.set("dataObject.results.accumulatedAmplitudes",accumulatedAmplitudes);
        }
    }

    _computeTensionAmplitudesLabeled(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedAmplitudesLabeled = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                let amplitude = Math.abs(v.max/2).toPrecision(4);
                return [v.name,"amplitude: "+amplitude,accumulatedAmount];
            });
            console.log("computed: ",{accumulatedAmplitudesLabeled});
            this.set("dataObject.results.accumulatedAmplitudesLabeled",accumulatedAmplitudesLabeled);
        }
    }*/

    _ArrayToText(array,...headerArray){
        if(!array || array.length <1){
            return;
        }
        let containsArrays = Array.isArray(array[0]);
        if(containsArrays){
            var headerString="";
            if(headerArray && headerArray.length>0){
                headerString = headerArray.join(";")+"\n";
            }
            return headerString+array.map(function(v){
                return v.join(";")+"\n";
            }).join("");
        }else{
            var firstElement = array[0];
            var keys = Object.keys(firstElement);
            console.log({keys,firstElement});
            var header = keys.join(";")+";\n";
            console.log({header});
            var body = array.map(row=>keys.map(key=>row[key]).join(";")+";\n").join("");
            console.log({body});
            return header.concat(body);
        }
    }

    _ArrayToIndexHeaders(array){
        if(array){
            return array.map(function(v,i){
                return i;
            });
        }
    }
}

window.customElements.define('my-rainflow-result', MyRainflowResult);