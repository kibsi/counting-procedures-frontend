/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import 'paper-collapse-item/paper-collapse-item.js';
import './my-horizontal-bar-chart.js';
import {MyConversionHelper} from './my-helpers.js';

class MyRangeCountResult extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
            }
        </style>
        <template is="dom-if" if="[[_exists(dataObject.results.rangeCountsLabeled)]]">
            <div class="card">
                <paper-collapse-item>
                    <h2 slot="header">Bereichs Häufigkeit</h2>
                    <paper-tabs selected="{{selectedTabIndexClassCrossings}}" scrollable>
                        <paper-tab>Chart</paper-tab>
                        <paper-tab>CSV</paper-tab>
                    </paper-tabs>
                    <iron-pages selected="[[selectedTabIndexClassCrossings]]">
                        <my-horizontal-bar-chart table-data="[[dataObject.results.rangeCountsLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.rangeCountsLabeled)]]"></my-horizontal-bar-chart>
                        <paper-textarea value="[[_arrayToText(dataObject.results.rangeCounts)]]"></paper-textarea>
                    </iron-pages>
                </paper-collapse-item>
            </div>
        </template>
        <template is="dom-if" if="[[_exists(dataObject.results.accumulatedCyclesLabeled)]]">
            <div class="card">
                <paper-collapse-item>
                    <h2 slot="header">Summenhäufigkeit</h2>
                    <paper-tabs selected="{{selectedTabIndexAccumulatedCycles}}" scrollable>
                        <paper-tab>Chart</paper-tab>
                        <paper-tab>CSV</paper-tab>
                    </paper-tabs>
                    <iron-pages selected="[[selectedTabIndexAccumulatedCycles]]">
                            <my-horizontal-bar-chart table-data="[[dataObject.results.accumulatedCyclesLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.accumulatedCyclesLabeled)]]"></my-horizontal-bar-chart>
                        <paper-textarea value="[[_ArrayToText(dataObject.results.accumulatedCycles,'name','max','min','amount')]]"></paper-textarea>
                    </iron-pages>
                </paper-collapse-item>
            </div>
        </template>
        <template is="dom-if" if="[[_exists(dataObject.results.accumulatedAmplitudesLabeled)]]">
            <div class="card">
                <paper-collapse-item>
                    <h2 slot="header">Spannungsamplituden</h2>
                    <paper-tabs selected="{{selectedTabIndexTensionAmplitudes}}" scrollable>
                        <paper-tab>Chart</paper-tab>
                        <paper-tab>CSV</paper-tab>
                    </paper-tabs>
                    <iron-pages selected="[[selectedTabIndexTensionAmplitudes]]">
                        <my-horizontal-bar-chart table-data="[[dataObject.results.accumulatedAmplitudesLabeled]]" height="[[_computeCalculationClassHeight(dataObject.results.accumulatedAmplitudesLabeled)]]"></my-horizontal-bar-chart>
                        <paper-textarea value="[[_arrayToText(dataObject.results.accumulatedAmplitudes,'name','amplitude','min','amount')]]"></paper-textarea>
                    </iron-pages>
                </div>
            </paper-collapse-item>
        </template>
        <template is="dom-if" if="[[_exists(dataObject.results.accumulatedAmplitudesLabeled)]]">
                <my-class-crossings-evaluation class-crossings-result-labeled="[[dataObject.results.accumulatedAmplitudesLabeled]]" class-crossings-result="[[dataObject.results.accumulatedAmplitudes]]"></my-class-crossings-evaluation>
        </template>
        <template is="dom-if" if="[[_exists(dataObject.results.accumulatedCyclesLabeled)]]">
                <my-class-crossings-evaluation class-crossings-result-labeled="[[dataObject.results.accumulatedCyclesLabeled]]" class-crossings-result="[[dataObject.results.accumulatedCycles]]"></my-class-crossings-evaluation>
        </template>
        `;
    }

    static get properties() {
        return {
            dataObject:{
                type: Object,
                value: {},
                notify: true,
            },
            selectedTabIndexClassCrossings:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexAccumulatedCycles:{
                type: Number,
                value: 0,
                notify: true,
            },
            selectedTabIndexTensionAmplitudes:{
                type: Number,
                value: 0,
                notify: true,
            }
        }
    }

    static get observers(){
        return [
            '_computeRangeCountsLabeled(dataObject.dataClasses)',
            '_computeRangeCounts(dataObject.dataClasses)',
            '_computeAccumulatedCycles(dataObject.dataClasses)',
            '_computeAccumulatedCyclesLabeled(dataObject.dataClasses)',
            //'_computeTensionAmplitudes(dataObject.dataClasses)',
            //'_computeTensionAmplitudesLabeled(dataObject.dataClasses)',
        ];
    }

    _computeCalculationClassHeight(table,changeRecord){
        //console.log("_computeCalculationClassHeight");
        //console.log(changeRecord.base);
        if(changeRecord){
            var base = changeRecord.base;
        }else{
            var base = table;
        }
        if(!base || base.length==0){
          return 0;
        }
        return ((base.length)*36)+100;
    }

    _exists(target){
        return target ? true : false;
    }

    _arrayToText(array,...headerArray){
        return MyConversionHelper.arrayToText(array,headerArray);
    }

    _computeRangeCountsLabeled(dataClasses){
        if(dataClasses){
            var rangeCountsLabeled = dataClasses.map(entry=>[entry.name,entry.maxLabel,entry.minLabel,entry.amount]);
            console.log("computed: ",{rangeCountsLabeled});
            this.set("dataObject.results.rangeCountsLabeled",rangeCountsLabeled);
        }
    }

    _computeRangeCounts(dataClasses){
        if(dataClasses){
            var rangeCounts = dataClasses.map(entry=>{return {name:entry.name,max:entry.max,min:entry.min,amount:entry.amount}});
            console.log("computed: ",{rangeCounts});
            this.set("dataObject.results.rangeCounts",rangeCounts);
        }
    }

    _computeAccumulatedCycles(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().sort((a,b)=>{
                if(Math.abs(a.max) == Math.abs(b.max)){
                    return b.max-a.max;
                }
                return Math.abs(b.max)-Math.abs(a.max);
            });
            console.log({reversed});
            var accumulatedCycles = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                return {name:v.name,max:v.max,min:v.min,amount:accumulatedAmount};
            });
            console.log("computed: ",{accumulatedCycles});
            this.set("dataObject.results.accumulatedCycles",accumulatedCycles);
        }
    }
    _computeAccumulatedCyclesLabeled(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().sort((a,b)=>{
                if(Math.abs(a.max) == Math.abs(b.max)){
                    return b.max-a.max;
                }
                return Math.abs(b.max)-Math.abs(a.max);
            });
            console.log({reversed});
            var accumulatedCyclesLabeled = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                return [v.name,v.minLabel,v.maxLabel,accumulatedAmount];
            });
            console.log("computed: ",{accumulatedCyclesLabeled});
            this.set("dataObject.results.accumulatedCyclesLabeled",accumulatedCyclesLabeled);
        }
    }

    _computeTensionAmplitudes(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedAmplitudes = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                let amplitude = Math.abs(v.max/2);
                return {name:v.name,max:amplitude,min:0,amount:accumulatedAmount};
            });
            console.log("computed: ",{accumulatedAmplitudes});
            this.set("dataObject.results.accumulatedAmplitudes",accumulatedAmplitudes);
        }
    }

    _computeTensionAmplitudesLabeled(dataClasses){
        if(dataClasses){
            var accumulatedAmount = 0;
            var reversed = dataClasses.slice().reverse();
            console.log({reversed});
            var accumulatedAmplitudesLabeled = reversed.map(function(v,i){
                accumulatedAmount = accumulatedAmount + v.amount;
                let amplitude = Math.abs(v.max/2).toPrecision(4);
                return [v.name,"Amplitude: "+amplitude,accumulatedAmount];
            });
            console.log("computed: ",{accumulatedAmplitudesLabeled});
            this.set("dataObject.results.accumulatedAmplitudesLabeled",accumulatedAmplitudesLabeled);
        }
    }

}



window.customElements.define('my-range-count-result', MyRangeCountResult);