/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */

class MyResultConverter {

    static arrayToObjects(keywordsArray,targetArray){
        return targetArray.map((v,i)=>{
            var entry = {};
            keywordsArray.forEach((key,pos) => {
                if(key){
                    if(v || v == 0){
                        if(Array.isArray(v) && v.length >pos){
                            entry[key] = v[pos];
                        }else{
                            entry[key] = v;
                        }
                    }
                }
            });
            return entry;
        })
    }

    static combineObjects(o1,o2){
        return o1.map((v1,i)=>{
            let v2 = o2[i];
            return {...v1,...v2};
        })
    }

    static objectsToArray(objectArray,fields,prefixes,postfixes,valueModifiers){
        if(!objectArray){
            return;
        }
        return objectArray.map(v=>{
            return fields.map((fieldName,i)=>{
                let prefix = '';
                if(prefixes&&prefixes.length>i&&prefixes[i]){
                    prefix = prefixes[i];
                }
                let postfix = '';
                if(postfixes&&postfixes.length>i&&postfixes[i]){
                    postfix = postfixes[i];
                }
                if(valueModifiers&&valueModifiers.length>i){
                    let valueModifier = valueModifiers[i];
                    if(valueModifier){
                        //console.log({valueModifier,result1:valueModifier(1),result2:valueModifier(2),param:v[fieldName],result:valueModifier(v[fieldName])});

                        var value = valueModifier(v[fieldName]);
                        //var modifiedValue = value;
                        return prefix+value+postfix;
                    }
                }
                return prefix+v[fieldName]+postfix;
            });
        });
    }
}



export {MyResultConverter};