/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import {DownloadHelper} from './my-helpers.js';

class MySimpleTable extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
                padding: 10px;
            }
        </style>
        <template is="dom-if" if="[[!_isEmpty(tableData)]]">
            <div class="next-line scrollable" id="datatable">
                <template is="dom-if" if="[[!hideHeadersLeft]]">
                    <table class="leftish">
                        <th>
                            <tr><td><b>Klasse</b></td></tr>
                            <tr><td><b>Max</b></td></tr>
                            <tr><td><b>Min</b></td></tr>
                        </th>
                        <template is="dom-repeat" items="[[leftHeader]]">
                            <tr><td><b>[[item]]:</b></td></tr>
                        </template>
                    </table>
                </template>
                <table class="leftish">
                    <template is="dom-repeat" items="[[topHeader]]">
                        <th>
                            <template is="dom-repeat" items="[[item]]">
                                <tr><td><b>[[item]]</b></td></tr>
                            </template>
                        </th>
                    </template>
                    <template is="dom-repeat" items="[[tableData]]">
                        <tr>
                            <template is="dom-repeat" items="[[item]]">
                                <template is="dom-if" if="[[_isZero(item)]]">
                                    <td class="zero-table-value">[[roundToDecimals(item)]]</td>
                                </template>
                                <template is="dom-if" if="[[!_isZero(item)]]">
                                    <td class="non-zero-table-value">[[roundToDecimals(item)]]</td>
                                </template>
                            </template>
                        </tr>
                    </template>
                    <template is="dom-if" if="[[_exists(tableData)]]">
                        <tr><slot name="content"></slot></tr>
                    </template>
                </table>
            </div>
            <div class="next-line">
                <paper-button id="saveSVG" class="blueish" on-click="saveSVG">SVG<iron-icon icon="file-download"/></paper-button>
                <paper-button id="savePNG" class="blueish" on-click="savePNG">PNG<iron-icon icon="file-download"/></paper-button>
                <paper-button id="saveCSV" class="blueish" on-click="saveCSV">CSV<iron-icon icon="file-download"/></paper-button>
            </div>
        </template>
        `;
    }

    static get properties() {
        return {
            tableData: {
                type: Array,
                //value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            tableHeader:{
                type: Array,
                //value: [],
                notify: true,
                //reflectToAttribute:true,
            },
            topHeader:{
                type: Array,
                notify: true,
            },
            leftHeader:{
                type: Array,
                notify: true,
            },
            hideHeadersLeft: {
                type: Boolean,
                value: false,
                notify: true,
                reflectToAttribute: true,
            },
            decimals: {
                type: Number,
                value: -1,
                notify: true,
                //reflectToAttribute:true,
            },
        }
    }

    static get observers(){
        return [
            '_toTopHeader(tableHeader,tableHeader.*)',
            '_toLeftHeader(tableHeader,tableHeader.*)'
          //'_handleResultsChange(result)',
          //'_markovProbabilitiesToString(markovProbabilities.*)',
        ];
    }

    _exists(element){
        return element ? true : false;
    }

    _isZero(value){
        if(value == 0){
            //console.log("zero");
            return true;
        }else{
            //console.log("not zero");
            return false;
        }
    }

    roundToDecimals(numberToRound){
        var decimals = this.get("decimals");
        if(decimals>=0){
            var multiplier = Math.pow(10,decimals);
            var result = Math.round(numberToRound*multiplier)/multiplier;
            return result;
        }
        return numberToRound;
    }

    saveCSV(){
        console.log("preparing to save CSV")
        DownloadHelper.saveCsv(this.get("tableData"));
    }

    saveSVG(){
        console.log("preparing to save SVG")
        DownloadHelper.saveHtmlAsSVG(this.shadowRoot.querySelector("#datatable"));
    }

    savePNG(){
        console.log("preparing to save PNG")
        DownloadHelper.saveHtmlAsPng(this.shadowRoot.querySelector("#datatable"));
    }

    _isEmpty(array,changeRecord){
        if(array==undefined){
          //console.log("_isEmpty undefined - true");
          return true;
        }
        if(array.length>0){
          //console.log("_isEmpty false");
          return false;
        }
        //console.log("_isEmpty true");
        return true;
    }

    _toTopHeader(base,changeRecord){
        var tableHeader = this.get("tableHeader");
        if(Array.isArray(tableHeader) && tableHeader.length>0){
            var topHeader = tableHeader.map((v,i)=>{
                if(Array.isArray(v) && v.length>0){
                    return v;
                }else{
                    return [v];
                }
            })
            this.set("topHeader",topHeader);
        }else{
            this.set("topHeader",[]);
        }
    }

    _toLeftHeader(base,changeRecord){
        var tableHeader = this.get("tableHeader");
        if(Array.isArray(tableHeader) && tableHeader.length>0){
            var leftHeader = tableHeader.map((v,i)=>{
                if(Array.isArray(v) && v.length>0){
                    return v[0];
                }else{
                    return v;
                }
            })
            this.set("leftHeader",leftHeader);
        }else{
            this.set("leftHeader",[]);
        }
    }
    
}

window.customElements.define('my-simple-table', MySimpleTable);