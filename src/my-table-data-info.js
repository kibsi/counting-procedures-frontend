/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
//import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/polymer/lib/elements/dom-bind.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-input/paper-textarea.js';
//import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
// Import template repeater
import '@polymer/polymer/lib/elements/dom-repeat.js';
//import '@polymer/polymer/lib/elements/dom-if.js';
// Import array selector
import '@polymer/polymer/lib/elements/array-selector.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-grid/all-imports.js';
import '@vaadin/vaadin-upload/vaadin-upload.js';
//import '@vaadin/vaadin-grid/vaadin-grid.js';
//import '@vaadin/vaadin-grid/vaadin-grid-column.js';
import './shared-styles.js';
import {MyFormatter} from './my-helpers.js';

class MyTableDataInfo extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
                padding: 10px;
            }
        </style>
        <div>
            <p>min=[[min]], max=[[max]]</p>
        </div>
        `;
    }

    static get properties() {
        return {
            tableData: {
                type: Array,
                value: [[]],
            },
            min: {
                type: Number,
                value: 0,
                notify: true,
                reflectToAttribute:true,
            },
            max: {
                type: Number,
                value: 0,
                notify: true,
                reflectToAttribute:true,
            },
        }
    }

    static get observers(){
        return [
          '_calculateBounds(tableData)',
        ];
    }

    _calculateBounds(tableData){
        console.log("_calculateBounds");
        //console.log("tableData",tableData);
        var currentValue;
        var tempMin = Number.MAX_VALUE;
        var tempMax = Number.MIN_VALUE;
        for(var i=0; i<tableData.length;i++){
            for(var j=0;j<tableData[i].length;j++){
                currentValue = tableData[i][j];
                if(typeof(currentValue) === 'string' || currentValue instanceof String){
                    currentValue = parseFloat(currentValue.replace(",","."));
                }
                if(currentValue<tempMin){
                    tempMin=currentValue;
                }
                if(currentValue>tempMax){
                    tempMax=currentValue;
                }
            }
        }
        
        this.set("min",Math.floor(tempMin));
        //console.log(tempMin);
        
        this.set("max",Math.ceil(tempMax));
        //console.log(tempMax);
        //tableData.
    }
    
};

window.customElements.define('my-table-data-info', MyTableDataInfo);