/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
//import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/polymer/lib/elements/dom-bind.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-input/paper-input-container.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-input/iron-input.js';
import '@polymer/iron-scroll-target-behavior/iron-scroll-target-behavior.js';
// Import template repeater
import '@polymer/polymer/lib/elements/dom-repeat.js';
//import '@polymer/polymer/lib/elements/dom-if.js';
// Import array selector
import '@polymer/polymer/lib/elements/array-selector.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@vaadin/vaadin-grid/all-imports.js';
//import '@vaadin/vaadin-upload/vaadin-upload.js';
//import '@vaadin/vaadin-grid/vaadin-grid.js';
//import '@vaadin/vaadin-grid/vaadin-grid-column.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import {MyFormatter,DownloadHelper} from './my-helpers.js';
import './shared-styles.js';
import './my-data-generator.js';

class MyTableDataView extends PolymerElement {
    static get template() {
        return html`
        <style include="shared-styles">
            :host {
                display: block;
                padding: 10px;
            }
        </style>
        <div>
            <paper-tabs selected="{{selectedTabIndex}}" scrollable>
                <paper-tab>Tabelle</paper-tab>
                <paper-tab>Text (CSV-Format)</paper-tab>
                <paper-tab>Datei hochladen</paper-tab>
                <paper-tab>Verlauf-Generator</paper-tab>
                <paper-tab>Beispieldaten</paper-tab>
            </paper-tabs>
            <iron-pages selected="[[selectedTabIndex]]">
                <div>
                    <vaadin-grid aria-label="source-data-table" items="{{tableValues}}" >

                        <vaadin-grid-column width="60px" flex-grow="0">
                            <template class="header">#</template>
                            <template>[[index]]</template>
                        </vaadin-grid-column>

                        <vaadin-grid-column>
                            <template class="header">[[_getTableHeader(tableHeaders.*,0)]]</template>
                            <template>
                            <input value="{{item.0::change}}"></input>
                            </template>
                        </vaadin-grid-column>

                        <!-- <dom-repeat items="{{tableHeaders}}" as="head">
                            <vaadin-grid-column>
                                <template class="header">{{head}}</template>
                                <template>
                                <input value="{{item.6::change}}"></input>
                                <!-- {{item.value::input}} -->
                                </template>
                            </vaadin-grid-column>
                        </dom-repeat> -->

                    </vaadin-grid>
                </div>
                <div>
                    <paper-textarea value="{{tableValuesAsText::value-changed}}" max-rows="10"></paper-textarea>
                    <paper-button class="blueish" on-click="saveDataAsCSV">CSV Speichern</paper-button>
                </div>
                
                <paper-input id="fileuploader" type="file" name="Select a csv file" id="my-file-selector" accept=".csv, .tsv" on-change="_fileChanged">
                    <!-- paper-button class="blueish" slot="prefix"><iron-icon icon="file-upload"/></paper-button -->
                </paper-input>
                <!-- paper-input-container>
                    <paper-button class="blueish" slot="label"><iron-icon icon="file-upload"/></paper-button>
                    <iron-input slot="input"><input class="blueish" type="file"/></iron-input>
                </paper-input-container -->
                <div>
                    <my-data-generator generated-data="{{tableValues}}"></my-data-generator>
                </div>
                <div id="scrollable-exampledata" class="scroll">
                    <x-element scroll-target="scrollable-exampledata">
                        <paper-listbox attr-for-selected="item-self" slot="dropdown-content" selected="{{selectedExample}}">
                            <template is="dom-repeat" items="{{examples}}">
                                <paper-item item-self="[[item]]" item-name="[[item.name]]">[[styleStringForTitle(item.name)]]</paper-item>
                            </template>
                        </paper-listbox>
                    </x-element>
                </div>
            </iron-pages>
        </div>
        `;
    }

    static get properties() {
        return {
            updatingMap: {
                type: Object,
                value: {
                    dataAsTextUpdate: {
                        type: Boolean,
                        value: false,
                        notify: true,
                        reflectToAttribute:true,
                    }
                },
                reflectToAttribute:true,
            },
            selectedTabIndex: {
                type: String,
                value() {
                  return '4';
                },
                //notify: true
            },
            tableHeaders: {
                type: Array,
                notify: true,
                reflectToAttribute:true,
            },
            usedHeaders : {
                type: Array,
                value() {
                    return ['<<undefined>>','<<undefined>>','<<undefined>>','<<undefined>>','<<undefined>>','<<undefined>>','<<undefined>>','<<undefined>>','<<undefined>>','<<undefined>>'];
                },
            },
            tableValues: {
                type: Array,
                //value() {
                //  return [{time:'5',value:"6"},{time:'10',value:'12'},{time:'15',value:'7'}];
                //},
                notify:true,
                reflectToAttribute:true,
                //observers :
            },
            tableValuesAsText:{
                type: String,
                value: '',
                reflectToAttribute:true,
                notify: true,
                //computed: '_arrayToCSV(tableValues.*)',
                //observers:
            },
            csvFiles: {
                type: Array
            },
            examples: {
                type: Array,
                value: [],
                notify: true,
                reflectToAttribute:true,
            },
            selectedExample: {
                type: Object,
                value: {
                    name: 'nothing-selected',
                },
                notify: true,
                reflectToAttribute:true,
            },
        }
    }

    static get observers(){
        return [
          '_arrayToCSV(tableValues.*)',
          '_CSVToArray(tableValuesAsText)',
          '_reactToFileUpload(csvFiles.*)',
        ];
    }

    styleStringForTitle(stringToStyle){
        return MyFormatter.styleStringForTitle(stringToStyle);
    }

    /*_triggerUpload(){
        console.log(this.shadowRoot.querySelector("#fileuploader").inputElement.inputElement);
        var event = new Event('click', {
            'bubbles': true,
            'cancelable': true
        });
        
        this.shadowRoot.querySelector("#fileuploader").inputElement.inputElement.dispatchEvent(event);
        //this.shadowRoot.querySelector("#fileuploader").inputElement.inputElement.fire("input");
    }*/

    _getTableHeader(access_token,index){
        var tableHeaders = this.get('tableHeaders');
        if(tableHeaders.length>index){
            return tableHeaders[index];
        }
        return 'Value';
    }

    _reactToFileUpload(fileElement){
        console.log('file uploaded!');
        //console.log(fileElement.base);
    }
    

    _arrayToCSV (changeRecord){
        var value = changeRecord.base;
        if(value.length==0){
          return;
        }
        //console.log(this.get('updatingMap.dataAsTextUpdate.value'));
        console.log('calling _arrayToCSV');
        if(!this.get('updatingMap.dataAsTextUpdate.value')){
          this.set('updatingMap.dataAsTextUpdate.value',true);
          
          
          //console.log(value);
          if(value===undefined){
            return '';
          }
          var result = '';
          var myArray = value;
          value.forEach(function(entry){
              result += entry.join(';');
              result += ';\n';
          });
          //myArray.forEach(function(entry){
          //  result += entry.time;
          //  result += ';';
          //  result += entry.value;
          //  result += ';\n';
          //  });
          this.set('tableValuesAsText',result);
          this.set('updatingMap.dataAsTextUpdate.value',false);
          return result;
        }
    };
    
    _CSVToArray(csvString){
        if(csvString==''){
          return;
        }
        console.log('calling _CSVToArray');
        //console.log(this.get('updatingMap'));
        //console.log(this.get('updatingMap.dataAsTextUpdate.value'));
        if(!this.get('updatingMap.dataAsTextUpdate.value')){
          this.set('updatingMap.dataAsTextUpdate.value',true);
          //console.log(this.get('updatingMap.dataAsTextUpdate.value'));
          csvString = csvString.split('\t').join(';');
          var linesArray =  csvString.split('\n');
          var resultArray = [];
          linesArray.forEach(function(entry){
            var values = entry.split(';')
            if(values[0]!=''){
                resultArray.push(values);
              //resultArray.push({time:values[0],value:values[1]});
            }
          });
          this.set('tableValues',resultArray);
          this.set('updatingMap.dataAsTextUpdate.value',false);
        return resultArray;
        }
    }
    _fileChanged(e){
        var files = e.target.inputElement.inputElement.files;
        //console.log(files);
        var that = this;
        if(files.length>0){
            console.log("file selected");
            var file = files[0];
            console.log(file);
            var reader = new FileReader();
            reader.onload = function(e){
                var content = e.target.result;
                that.set("tableValuesAsText",content);
            }
            reader.readAsText(file);
            //console.log("end selected");
        }
    }

    saveDataAsCSV(){
        var textToSave = this.get("tableValuesAsText");
        DownloadHelper.saveCsv(textToSave);
    }
    
};

window.customElements.define('my-table-data-view', MyTableDataView);