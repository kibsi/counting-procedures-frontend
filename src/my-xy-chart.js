/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import * as d3 from "d3";
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import {DownloadHelper} from './my-helpers.js';
import {MyResultConverter} from './my-result-converter.js';


class MyXYChart extends PolymerElement {
    static get template() {
      return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
      </style>
      <div id="my-containing-div"><svg id="dataChart"></svg></div>
      <div>
        <paper-input label="min" value="{{startPointString}}" class="horizontal"></paper-input>
        <paper-input label="max" value="{{endPointString}}" class="horizontal"></paper-input>
        <paper-input label="red marker size" value="{{redMarkerSize}}" class="horizontal" hidden="[[redMarkerHidden]]"></paper-input>
        <paper-input label="blue marker size" value="{{blueMarkerSize}}" class="horizontal" hidden="[[blueMarkerHidden]]"></paper-input>
        <paper-input label="black marker size" value="{{blackMarkerSize}}" class="horizontal"></paper-input>
      </div>
      <div>
        <p class="reddish">[[warning]]</p>
      </div>
      <div>
          <paper-button id="saveSVG" class="blueish" on-click="saveSVG">SVG<iron-icon icon="file-download"/></paper-button>
          <paper-button id="savePNG" class="blueish" on-click="savePNG">PNG<iron-icon icon="file-download"/></paper-button>
          <paper-button id="saveCSV" class="blueish" on-click="saveCSV">CSV<iron-icon icon="file-download"/></paper-button>
      </div>
      `;
    }

    constructor(){
      super();
      window.addEventListener('resize', (e) => {this._handleResize()});
    }

    static get properties() {
        return {
          dataObject:{
            type: Object,
            value: {},
            notify: true,
          },
            width: {
                type: String,
                value: "-1",
                //notify: true,
                ////reflectToAttribute:true,
            },
            height: {
                type: String,
                value: "-1",
                //notify: true,
                ////reflectToAttribute:true,
            },
            
            //Marker
            redMarkerHidden:{
              type: Boolean,
              value: true,
              notify: true,
              //reflectToAttribute:true,
            },
            redMarkerSize:{
              type: Number,
              value: 2,
              notify: true,
              //reflectToAttribute:true,
            },
            blueMarkerHidden:{
              type: Boolean,
              value: true,
              notify: true,
              //reflectToAttribute:true,
            },
            blueMarkerSize:{
              type: Number,
              value: 1,
              notify: true,
              //reflectToAttribute:true,
            },
            blackMarkerSize:{
              type: Number,
              value: 1,
              notify: true,
              //reflectToAttribute:true,
            },
            //Zoom
            startPoint:{
              type: Number,
              value: 0,
            },
            endPoint:{
              type: Number,
              value: 0,
            },
            startPointString:{
              type: String,
              value: 0,
            },
            endPointString:{
              type: String,
              value: 0,
            },
            warning: {
              type: String,
              value: "",
            }
        };
    }

    static get observers(){
        return [
            '_limitSize(dataObject)',
            '_refreshChart(dataObject)',
            '_refreshChart(width)',
            '_refreshChart(height)',
            '_setStartZeroIfEmpty(startPointString)',
            '_setEndZeroIfEmpty(endPointString)',
            '_refreshChart(startPoint)',
            '_refreshChart(endPoint)',
            '_refreshChart(redMarkerSize)',
            '_refreshChart(blueMarkerSize)',
            '_refreshChart(blackMarkerSize)',
        ];
    }

    _limitSize(dataObject){
      if(dataObject && dataObject.lines){
        if(dataObject.lines.length>1000){
          this.set("startPointString",0);
          this.set("endPointString",1000);
          this.set("warning","Der Verlauf hat mehr als 1000 Punkte. Wenn mehr als 1000 Punkte angezeigt werden, kann das die Homepage sehr langsam machen!");
        }else{
          this.set("warning","");
        }
      }
    }

    _handleResize(){
      //console.log("resize");
      this._refreshChart("resize");
    }

    _setStartZeroIfEmpty(changeRecord){
      if(changeRecord == ''){
        this.set("startPoint",0);
      }else{
        this.set("startPoint",changeRecord);
      }
    }

    _setEndZeroIfEmpty(changeRecord){
      if(changeRecord == ''){
        this.set("endPoint",0);
      }else{
        this.set("endPoint",changeRecord);
      }
    }
    
      _refreshChart(changeRecord){
        //console.log("_refreshChart");
        var dataObject = this.get("dataObject");
        if(!dataObject){
          return;
        }
        var lines = dataObject.lines;
        if(!lines || lines.length<2){
          return;
        }
        //console.log("refreshing xy-chart with data:",{dataObject});
        var markedIndizes = dataObject.usedIndizes;
        if(markedIndizes instanceof Array && markedIndizes.length>0){
          //this.shadowRoot.querySelector("#red-label-marker-input").setAttribute("hidden","false");
          this.set("redMarkerHidden",false);
          //console.log("red markers available");
        }else{
          markedIndizes = [];
          //this.shadowRoot.querySelector("#red-label-marker-input").setAttribute("hidden","true");
          this.set("redMarkerHidden",true);
          //console.log("no red markers",markedIndizes);
        }
        var pointsOfInterest = dataObject.pointsOfInterest;
        if(pointsOfInterest instanceof Array && pointsOfInterest.length>0){
          //this.shadowRoot.querySelector("#blue-label-marker-input").setAttribute("hidden","false");
          this.set("blueMarkerHidden",false);
          //console.log("blue markers available");
        }else{
          pointsOfInterest=[];
          //this.shadowRoot.querySelector("#blue-label-marker-input").setAttribute("hidden","true");
          this.set("blueMarkerHidden",true);
          //console.log("no blue markers",pointsOfInterest);
        }
        
        var shadowSvg = this.shadowRoot.querySelector("#dataChart");
        var containingDiv = this.shadowRoot.querySelector("#my-containing-div");
        var xyWidth = parseFloat(this.get("width"));
        var xyHeight = parseFloat(this.get("height"));
        var margin = {top: 20, right:80, bottom:30, left:50};
        var startPoint = parseFloat(this.get("startPoint"));
        var endPoint = parseFloat(this.get("endPoint"));
        var redMarkerSize = parseFloat(this.get("redMarkerSize"));
        var blueMarkerSize = parseFloat(this.get("blueMarkerSize"));
        var blackMarkerSize = parseFloat(this.get("blackMarkerSize"));
        if(xyWidth<0){
          xyWidth = containingDiv.offsetWidth;
        }
        if(xyWidth<margin.left + margin.right){
          xyWidth = 1200;
        }
        if(xyHeight<0){
          xyHeight = containingDiv.offsetHeight;
        }
        if(xyHeight<margin.top + margin.bottom){
          xyHeight = 800;
        }
        shadowSvg.setAttribute("width",xyWidth);
        shadowSvg.setAttribute("height",xyHeight);
        var svg = d3.select(shadowSvg);
        // delete old content
        svg.html(null);
        var width = xyWidth - margin.left - margin.right;
        var height = xyHeight -margin.top - margin.bottom;
        var g = svg.append("g").attr("transform","translate("+margin.left+","+margin.top+")");
        
        var x = d3.scaleLinear().range([0,width]);
        var y = d3.scaleLinear().range([height,0]);
        var z = d3.scaleOrdinal(d3.schemeCategory10);
    
        /*var line = d3.line()
        .x(function(dataPoint,index) {return x(dataPoint.x);})
        .y(function(dataPoint,index) {return y(dataPoint.y);});*/
        
        let highestIndex = lines.length -1;
        //Zoom

        if(startPoint&&startPoint>0&&startPoint<highestIndex){
          var zoomStart = startPoint;
        }else{
          var zoomStart = 0;
        }

        if(endPoint&&endPoint>0&&endPoint<highestIndex&&endPoint>startPoint){
          var zoomEnd = endPoint;
        }else{
          var zoomEnd = highestIndex;
        }

        if(zoomStart>zoomEnd){
          zoomStart=0;
        }
        var xDomain = [lines[zoomStart].x1,lines[zoomEnd].x2];
        var scaledLines = lines.slice(zoomStart,zoomEnd+1);
        var scaledDataPoints = dataObject.dataPoints.slice(zoomStart,zoomEnd+1);
        //console.log({scaledLines});
        var yLines = dataObject.yLines;
        var maxOfYLines;
        var minOfYLines;
        if(!yLines){
          yLines = [];
          maxOfYLines = 0;
          minOfYLines = 0;
        }else{
          maxOfYLines = d3.max(yLines);
          minOfYLines = d3.min(yLines);
        }

        x.domain(xDomain);

        var yDomain = [
          d3.min([d3.min(scaledLines,line=>d3.min([line.y1,line.y2])),minOfYLines]),
          d3.max([d3.max(scaledLines,line=>d3.max([line.y1,line.y2])),maxOfYLines])
        ];
        //console.log({xDomain,yDomain});

        y.domain(yDomain);

        z.domain(scaledLines.map(function(line){return line.header;}));
        //Draw x-axis
        g.append("g")
          .attr("class", "axis axis--x")
          .attr("transform", "translate(0," + height + ")")
          .call(d3.axisBottom(x))
          .append("text")
          .attr("dx", "50.00em")
          .attr("dy", "3.00em")
          .attr("fill", "#000")
          .text("N");
        //Draw y-axis
        g.append("g")
          .attr("class", "axis axis--y")
          .call(d3.axisLeft(y))
          .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "-4.00em")
          .attr("dx", "-10.00em")
          .attr("fill", "#000")
          .text("Value");
        //Draw horizontall data class lines
        g.append("g")
          .attr("id","h-lines")
          .selectAll("line")
          .data(yLines)
          .enter()
          .append("line")
          .attr("x1",0)
          .attr("y1",dataPoint=>y(dataPoint))
          .attr("x2",width)
          .attr("y2",dataPoint=>y(dataPoint))
          .style("fill","none")
          .style("stroke","lightgrey")
          .style("stroke-width","1px");
        //Draw lines for data
        g.append("g")
          .attr("id","data-points")
          .selectAll("line")
          .data(scaledLines)
          .enter()
          .append("line")
          .attr("x1",myLine=>x(myLine.x1))
          .attr("y1",myLine=>y(myLine.y1))
          .attr("x2",myLine=>x(myLine.x2))
          .attr("y2",myLine=>y(myLine.y2))
          .style("stroke",myLine=>z(myLine.header));
        
        if(redMarkerSize){
          var sizeCrossX = 0.003;
          var sizeCrossY = sizeCrossX*width/height;
          var scaledx = (xDomain[1]-xDomain[0])*sizeCrossX;
          var scaledy = (yDomain[1]-yDomain[0])*sizeCrossY;
          var scaledIndizes = markedIndizes.filter(point=>point.x>=zoomStart&&point.x<=zoomEnd);
          //console.log({scaledIndizes});
          if(scaledIndizes.length){
            //console.log("scaledIndizes=",scaledIndizes);
            //console.log("Indizes to mark!");
            var marks = g.append("g")
            .attr("id","red-markers");
            marks.selectAll("line")
              .data(scaledIndizes)
              .enter()
              .append("line")
              .attr("x1",redMarkerPoint=>x(redMarkerPoint.x)-redMarkerSize)
              .attr("y1",redMarkerPoint=>y(redMarkerPoint.y)-redMarkerSize)
              .attr("x2",redMarkerPoint=>x(redMarkerPoint.x)+redMarkerSize)
              .attr("y2",redMarkerPoint=>y(redMarkerPoint.y)+redMarkerSize)
              .style("stroke","red")
              .style("stroke-width",redMarkerSize+"px")
              .append("svg:title").text(function(redMarkerPoint,i){
                return "["+redMarkerPoint.x+","+redMarkerPoint.y+"]";
              });
              var marks2 = g.append("g")
            .attr("id","red-markers");
              marks2.selectAll("line")
              .data(scaledIndizes)
              .enter()
              .append("line")
              .attr("x1",redMarkerPoint=>x(redMarkerPoint.x)+redMarkerSize)
              .attr("y1",redMarkerPoint=>y(redMarkerPoint.y)-redMarkerSize)
              .attr("x2",redMarkerPoint=>x(redMarkerPoint.x)-redMarkerSize)
              .attr("y2",redMarkerPoint=>y(redMarkerPoint.y)+redMarkerSize)
              .style("stroke","red")
              .style("stroke-width",redMarkerSize+"px")
              .append("svg:title").text(function(redMarkerPoint,i){
                return "["+redMarkerPoint.x+","+redMarkerPoint.y+"]";
              });
          }
        }
        if(blueMarkerSize){
          var scaledPointsOfInterest = pointsOfInterest.filter(function(value){
            if(value.length<2){
              return false;
            }
            for(var i=0;i<value.length;i=i+2){
              if(value[i]<zoomStart||value[i]>zoomEnd||!(value[i+1].toFixed)){
                return false;
              }
            }
            return true;
          });
          if(scaledPointsOfInterest.length){
            //console.log({scaledPointsOfInterest});
            var marks2 = g.append("g")
            .attr("id","blue-markers")
              .selectAll("line")
              .data(scaledPointsOfInterest)
              .enter()
              .append("line")
              .attr("x1",d=>x(d[0]))
              .attr("y1",d=>y(d[1]))
              .attr("x2",d=>x(d.length>3?d[2]:d[0]))
              .attr("y2",d=>y(d.length>3?d[3]:d[1]))
              .style("stroke","blue")
              .style("stroke-width",blueMarkerSize+"px")
              .append("svg:title").text(function(d,i){
                return "["+d[0]+","+d[1]+"]"+(d.length>3 ? "["+d[2]+","+d[3]+"]" : "");
              });
          }
        }
          if(blackMarkerSize){
          var circles = g.append("g")
          .attr("id","black-markers")
          .selectAll("circle")
          .data(scaledDataPoints)
          .enter()
          .append("circle")
          .attr("cx",d=>x(d.x))
          .attr("cy",d=>y(d.y))
          .attr("r",d=>blackMarkerSize)
          .style("fill","black")
          .on("mouseover",function(d){
            var self = d3.select(this);
            self.attr("r",blackMarkerSize*2+2);
          }).on("mouseout",function(d){
            var self = d3.select(this);
            self.attr("r",blackMarkerSize);
          }).append("svg:title").text(function(d){return "["+d.x+";"+d.y+"]"});
        }
      }
  saveSVG(){
    DownloadHelper.saveSVG(this.shadowRoot.querySelector("#dataChart"));
  }

  savePNG(){
    DownloadHelper.saveSvgAsPng(this.shadowRoot.querySelector("#dataChart"));
  }

  saveCSV(){
    DownloadHelper.saveCsv(MyResultConverter.objectsToArray(this.get("dataObject.dataPoints"),["y"]));
  }
}

window.customElements.define('my-xy-chart', MyXYChart);