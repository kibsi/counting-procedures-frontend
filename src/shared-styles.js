/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import '@polymer/polymer/polymer-element.js';

const $_documentContainer = document.createElement('template');
$_documentContainer.innerHTML = `<dom-module id="shared-styles">
  <template>
    <style>`
     + getMyGlobalStyle() + 
    `</style>
  </template>
</dom-module>`;

export function getMyGlobalStyle(){
  return `
  .card {
    margin-top: 12px;
    margin-bottom: 12px;
    margin-left: 6px;
    margin-right: 6px;
    padding-top: 6px;
    padding-bottom: 6px;
    padding-left: 6px;
    padding-right: 6px;
    color: #757575;
    border-radius: 5px;
    background-color: #fff;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
    max-height: 50%;
    overflow:auto;
  }

  .scroll{
    overflow: auto;
    max-height: 250px;
  }

  .innercard {
    margin: 2px;
    padding: 2px;
    color: #757575;
    border-radius: 5px;
    background-color: #fff;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
    max-height: 50%;
    overflow:auto;
  }

  .configurationwrapper{
    align: right;
    top:0;
  }

  .configuration {
    display: inline-block;
    text-align: top-left;
  }

  .blueish {
    color: #fff;
    border-radius: 5px;
    background-color: #0077ff;
    padding: 16px;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
  }

  .reddish{
    color: #f00;
  }

  .space-left{
    margin-left: 5px;
  }

  paper-tabs{
    background-color: #0077ff;
    border-radius: 5px;
  }

  
  paper-tab{
    color: white;
    secondary-color: red;
    accent-color: green;
    divider-color: orange;
    /* border-radius: 5px; */
    /* background-color: #99eeff; */
  }
  paper-tab.iron-selected {
    background-color: #0077ff;
    color: white;
  }

  .selectable, paper-item{
    transform-origin: 0% 50%;
    border-radius: 5px;
    
  }

  .help{
    cursor: help;
  }

  .selectable:hover, paper-item:hover{
    color: #fff;
    background-color: #0077ff;
    cursor: pointer;
  }

  .blueish-mini {
    color: #fff;
    border-radius: 15px;
    transform: scale(0.6);
    background-color: #0077ff;
    /* padding: 1px; */
    box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 2px 1px -1px rgba(0, 0, 0, 0.2);
  }

  .circle {
    display: inline-block;
    width: 64px;
    height: 64px;
    text-align: center;
    color: #555;
    border-radius: 50%;
    background: #ddd;
    font-size: 30px;
    line-height: 64px;
  }

  h1 {
    margin: 16px 0;
    color: #212121;
    font-size: 22px;
  }

  .horizontal {
    min-width: 100px;
    display: inline-block;
  }

  .left-part {
    width: 70%;
    max-width: 500px;
    float: left;
    display: inline-block;
  }

  .right-part {
    width: 30%;
    max-width: 200px;
    float: left;
    display: inline-block;
  }

  .grid-container {
    display: grid;
    grid-template-columns: 25% 25% 25% 25%;
    grid-template-areas:
      "top top top top"
      "center center center center"
      "bottom bottom bottom bottom";
  }

  .grid-top {
    grid-area: top;
    display: inline-grid;
    grid-template-rows: 1fr;
    gird-template-columns: auto;
  }

  .grid-center {
  }

  .grid-left {
    grid-area: left;
    display: inline-grid;
    grid-template-rows: auto;
    gird-template-columns: 1fr;
  }

  .grid-right {
    grid-area: right;
    display: inline-grid;
    grid-template-rows: auto;
    gird-template-columns: 1fr;
  }

  .grid-bottom {
    grid-area: bottom;
    grid-template-rows: 1fr;
    gird-template-columns: 1fr 1fr 1fr 1fr;
  }

  .linewrap{
    display: inline-block;
  }

  .next-line {
    display: inline-block;
    width:100%;
  }

  .horizontal50 {
    width: 50%;
    display: inline-block;
  }

  .zero-table-value{
    color: LightGray;
  }

  .non-zero-table-value{
    color: Gray;
  }

  .leftish {
    float: left;
    
    /* display: inline-block; */
    /* vertical-align: top; */
    
  }
  .scrollable {
      overflow:auto;
      display: flex;
  }
  td {
    text-align: center;
      width: 45px;
  }
  
  `;
}

//export {getMyGlobalStyle};

document.head.appendChild($_documentContainer.content);
